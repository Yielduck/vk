#include <vk/buffer.h>
#include <vk/command_buffer.h>
#include <vk/command_pool.h>
#include <vk/compute_pipeline.h>
#include <vk/descriptor_set.h>
#include <vk/descriptor_set_layout.h>
#include <vk/descriptor_pool.h>
#include <vk/device.h>
#include <vk/device_memory.h>
#include <vk/fence.h>
#include <vk/image.h>
#include <vk/image_view.h>
#include <vk/instance.h>
#include <vk/pipeline_layout.h>
#include <vk/physical_device_info.h>
#include <vk/shader_module.h>
#include <vk/queue_submit.h>

#include <vk/cmd/copy.h>
#include <vk/cmd/pipeline_barrier.h>
#include <vk/guard/memory_map.h>
#include <vk/guard/command_buffer.h>
#include <vk/khr/pipeline_executable_info.h>
#include <vk/ext/find.h>
#include <vk/ext/trivial/device.h>
#include <vk/ext/trivial/executor.h>
#include <vk/ext/trivial/instance.h>

#include <cmath>
#include <cassert>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <shader.h>

unsigned int const width = 1920;
unsigned int const height = 1080;
struct RGBA8
{
    unsigned char r, g, b, a;
};

struct Sphere
{
    alignas(16) float pos[4]; // (vec3 pos; float R;)
    alignas(16) float albedo[3];
};
struct Camera
{
    alignas(16) float pos[3];
    alignas(16) float up[3];
    alignas(16) float at[3];
    float tanFOV;
    float aspectRatio;
};
struct UniformBlock
{
    Sphere sphere[2];
    Camera camera;
};

int main()
{
    auto const
    [
        instance,
        physicalDevice,
        deviceInfo,
        device,
        genericQueueFamily,
        computeQueueFamily,
        graphicsQueueFamily,
        queueFamilyRelations,
        genericQueue,
        computeQueue,
        graphicsQueue
    ] = vk::ext::trivial::Device
    ({
        .instance = vk::ext::trivial::Instance
        ({
            .apiVersion = VK_API_VERSION_1_1,
            //.validationLayer = {{"VK_LAYER_KHRONOS_validation"}},
        }),
#ifdef VK_KHR_pipeline_executable_properties
        .features =
        {
            VkPhysicalDeviceVulkan11Features{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES},
            VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR},
        },
        .optionalExtensions = {{"VK_KHR_pipeline_executable_properties"}},
#endif
    });

    auto const findMemoryIndex = vk::ext::find::memoryIndex(deviceInfo.memoryProperties);

    VkDeviceSize const imageSize = sizeof(RGBA8) * width * height;
    vk::Image const image = vk::createImage
    (
        device,
        {
            .type = VK_IMAGE_TYPE_2D,
            .format = VK_FORMAT_R8G8B8A8_UINT,
            .extent = VkExtent3D{width, height, 1},
            .usage = VK_IMAGE_USAGE_STORAGE_BIT
                   | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
        }
    );
    VkMemoryRequirements const imageReq = vk::imageMemoryRequirements(device, image);
    vk::DeviceMemory const imageMemory = vk::createDeviceMemory
    (
        device,
        {
            .allocationSize = imageReq.size,
            .memoryTypeIndex = findMemoryIndex(vk::ext::predicate::deviceLocal, imageReq.memoryTypeBits).front(),
        }
    );
    vk::bindImageMemory(device, image, imageMemory);

    vk::ImageView const imageView = vk::createImageView
    (
        device,
        {
            .image = image,
            .type = VK_IMAGE_VIEW_TYPE_2D,
            .format = VK_FORMAT_R8G8B8A8_UINT,
        }
    );

    vk::Buffer const imageCopyBuffer = vk::createBuffer(device, {.size = imageSize, .usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT});
    VkMemoryRequirements const imageCopyBufferReq = vk::bufferMemoryRequirements(device, imageCopyBuffer);
    vk::DeviceMemory const imageCopyBufferMemory = vk::createDeviceMemory
    (
        device,
        {
            .allocationSize = imageCopyBufferReq.size,
            .memoryTypeIndex = findMemoryIndex(vk::ext::predicate::hostVisible, imageCopyBufferReq.memoryTypeBits).front(),
        }
    );
    vk::bindBufferMemory(device, imageCopyBuffer, imageCopyBufferMemory);

    VkDeviceSize const bufferSize = sizeof(UniformBlock);
    vk::Buffer const buffer = vk::createBuffer
    (
        device,
        {
            .size = bufferSize,
            .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
                   | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        }
    );
    VkMemoryRequirements const bufferReq = vk::bufferMemoryRequirements(device, buffer);
    vk::DeviceMemory const bufferMemory = vk::createDeviceMemory
    (
        device,
        {
            .allocationSize = bufferReq.size,
            .memoryTypeIndex = findMemoryIndex(vk::ext::predicate::hostVisible, bufferReq.memoryTypeBits).front(),
        }
    );
    vk::bindBufferMemory(device, buffer, bufferMemory);

    vk::DescriptorPool const descriptorPool = vk::createDescriptorPool
    (
        device,
        {
            .maxSets = 1u,
            .poolSize =
            {{
                {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE , 1u},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1u},
            }},
        }
    );

    vk::DescriptorSetLayout const descriptorSetLayout = vk::createDescriptorSetLayout
    (
        device,
        {
            .binding = 
            {{
                {
                    .binding = 0u,
                    .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                    .descriptorCount = 1u,
                    .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                    .pImmutableSamplers = nullptr,
                },
                {
                    .binding = 1u,
                    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = 1u,
                    .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                    .pImmutableSamplers = nullptr,
                },
            }}
        }
    );

    vk::DescriptorSet const descriptorSet = vk::createDescriptorSet
    (
        device,
        {
            .descriptorPool = descriptorPool,
            .setLayout = descriptorSetLayout,
        }
    );

    VkDescriptorImageInfo const descriptorImageInfo =
    {
        .sampler = VK_NULL_HANDLE,
        .imageView = imageView,
        .imageLayout = VK_IMAGE_LAYOUT_GENERAL,
    };
    VkDescriptorBufferInfo const descriptorBufferInfo =
    {
        .buffer = buffer,
        .offset = 0,
        .range = sizeof(UniformBlock),
    };
    vk::updateDescriptorSets
    (
        device,
        {{
            vk::writeDescriptorSet::storageImage (descriptorSet, 0u, {{descriptorImageInfo}}),
            vk::writeDescriptorSet::uniformBuffer(descriptorSet, 1u, {{descriptorBufferInfo}}),
        }}
    );

    vk::PipelineLayout const pipelineLayout = vk::createPipelineLayout(device, {.setLayout = {{descriptorSetLayout}}});

    vk::ShaderModule const computeShader = vk::createShaderModule
    (
        device,
        {
            .code = {shaderCode}
        }
    );
    VkExtent2D const fbExtent = {width, height};
    vk::ComputePipeline const pipeline = vk::createComputePipeline
    (
        device,
        {
#ifdef VK_KHR_pipeline_executable_properties
            .flags = VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR,
#endif
            .stage = vk::pipeline::ShaderStageCreateInfo
            ({
                .stage = VK_SHADER_STAGE_COMPUTE_BIT,
                .module = computeShader,
                .mapEntry =
                {{
                    {.constantID = 0, .offset = offsetof(VkExtent2D, width ), .size = sizeof(vk::u32)},
                    {.constantID = 1, .offset = offsetof(VkExtent2D, height), .size = sizeof(vk::u32)},
                }},
                .data = &fbExtent,
            }),
            .layout = pipelineLayout,
        }
    );

#ifdef VK_KHR_pipeline_executable_properties
    for(auto const &info : vk::khr::pipelineExecutableInfo(device, pipeline))
    {
        std::string const msg = info.properties.name
                              + std::string(" (")
                              + info.properties.description
                              + std::string("), subgroupSize = ")
                              + std::to_string(info.properties.subgroupSize);
        std::cout << msg << std::endl;
        for(auto const &stat : info.statistics)
        {
            std::string message = stat.name + std::string(" (") + stat.description + std::string(") = ");
            switch(stat.format)
            {
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR:    message += (stat.value.b32 ? "true" : "false"); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR:     message += std::to_string(stat.value.i64); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR:    message += std::to_string(stat.value.u64); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR:   message += std::to_string(stat.value.f64); break;
                default: assert(0);
            }
            std::cout << message << std::endl;
        }
        std::cout << std::endl;
    }
#endif

    vk::ext::trivial::Executor const executor
    ({
        .device = device,
        .queueFamily = computeQueueFamily,
        .queue = computeQueue,
    });
    executor.execute([&](VkCommandBuffer const commandBuffer)
    {
        float const dist = 1.f;
        float const phi = 0.f;
        float const theta = 0.4f;
        UniformBlock const block =
        {
            .sphere =
            {
                {
                    {0.f, 0.f, -1000.f, 1000.f},
                    {0.3f, 0.8f, 0.4f},
                },
                {
                    {0.f, 0.f, 0.2f, 0.5f},
                    {1.f, 1.f, 1.f},
                },
            },
            .camera =
            {
                .pos =
                {
                    std::cos(theta) * std::cos(phi) * dist,
                    std::cos(theta) * std::sin(phi) * dist,
                    std::sin(theta) * dist,
                },
                .up = {0.f, 0.f, 1.f},
                .at = {0.f, 0.f, 0.3f},
                .tanFOV = 1.f,
                .aspectRatio = static_cast<float>(width) / height,
            }
        };

        vkCmdUpdateBuffer(commandBuffer, buffer, 0, sizeof(block), &block);

        vk::cmd::pipelineBarrier
        (
            commandBuffer,
            {
                .dstStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                .imageBarrier =
                    {{ {
                        .dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
                        .image = image,
                    }} },
            }
        );

        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
        vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);
        vkCmdDispatch(commandBuffer, (width + 31u) / 32u, height, 1u);

        vk::cmd::pipelineBarrier
        (
            commandBuffer,
            {
                .srcStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT,
                .imageBarrier =
                    {{ {
                        .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
                        .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_GENERAL,
                        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                        .image = image,
                    }} },
            }
        );
        vk::cmd::copyImageToBuffer
        (
            commandBuffer,
            {
                .srcImage = image,
                .srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                .dstBuffer = imageCopyBuffer,
                .region = 
                {{ {
                    0, 0, 0,
                    {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1},
                    VkOffset3D{0, 0, 0},
                    VkExtent3D{width, height, 1},
                }} },
            }
        );
    });

    std::ofstream out("pic.ppm");
    out << "P6" << std::endl;
    out << width << " " << height << std::endl;
    out << "255" << std::endl;
    {
        vk::guard::MemoryMap<RGBA8 const> const guard({device, imageCopyBufferMemory});
        RGBA8 const * const colorData = guard.cref();
        for(unsigned int i = 0; i < height; ++i)
            for(unsigned int j = 0; j < width; ++j)
                out.write(reinterpret_cast<char const *>(colorData + i * width + j), 3);
    }
}
