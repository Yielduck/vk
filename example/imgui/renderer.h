#pragma once
#include <vk/shader_module.h>
#include <vk/pipeline_layout.h>
#include <vk/graphics_pipeline.h>

#include <chrono>

#include "shader/vs.h"
#include "shader/fs.h"

struct Renderer
{
    std::chrono::time_point<std::chrono::steady_clock> now0;

    vk::PipelineLayout          graphicsPipelineLayout;
    vk::GraphicsPipeline        graphicsPipeline;

    struct Push
    {
        vk::f32 time;
    };

    Renderer(VkDevice const device, VkRenderPass const renderPass)
        : now0(std::chrono::steady_clock::now())
    {
        graphicsPipelineLayout = vk::createPipelineLayout
        (
            device,
            {
                .pushConstantRange =
                {{
                    {
                        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                        .offset = 0,
                        .size = sizeof(Push),
                    }
                }}
            }
        );

        vk::ShaderModule const   vertexShader = vk::createShaderModule(device, {.code = {vs}});
        vk::ShaderModule const fragmentShader = vk::createShaderModule(device, {.code = {fs}});
        graphicsPipeline = vk::createGraphicsPipeline
        (
            device,
            {
                .stage =
                {{
                    vk::pipeline::ShaderStageCreateInfo
                    {
                        .stage = VK_SHADER_STAGE_VERTEX_BIT,
                        .module = vertexShader,
                    },
                    vk::pipeline::ShaderStageCreateInfo
                    {
                        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
                        .module = fragmentShader,
                    },
                }},
                .inputAssemblyState =
                {
                    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                },
                .viewportState      =
                {
                    .viewport = {{ VkViewport{} }},
                    .scissor  = {{ VkRect2D  {} }},
                },
                .colorBlendState    =
                {
                    .attachment = {{ vk::pipeline::ColorBlendAttachmentState{} }},
                },
                .dynamicState       =
                {
                    .dynamicState =
                    {{
                        VK_DYNAMIC_STATE_VIEWPORT,
                        VK_DYNAMIC_STATE_SCISSOR,
                    }}
                },
                .layout = graphicsPipelineLayout,
                .renderPass = renderPass,
            }
        );
    }

    void render(VkCommandBuffer const cmd) noexcept
    {
        VkPipelineBindPoint const bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        vkCmdBindPipeline(cmd, bindPoint, graphicsPipeline);

        std::chrono::duration<vk::f32, std::ratio<1>> const dt = std::chrono::steady_clock::now() - now0;
        Push const push =
        {
            .time = dt.count(),
        };
        vkCmdPushConstants(cmd, graphicsPipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(Push), &push);

        vkCmdDraw(cmd, 6, 1, 0, 0);
    }
};
