#version 460

layout(push_constant) uniform Push
{
    layout(offset = 0) float time;
} push;

layout(location = 0) in vec2 pos;
layout(location = 0) out vec4 outColor;

void main()
{
    outColor = vec4(pos, 0.5f + 0.5f * sin(push.time), 1.f);
}
