#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using DescriptorSet = vk::utils::UniqueHandle<VkDescriptorSet>;

struct DescriptorSetCreateInfo
{
    vk::utils::StructureChain   chain               = {};
    VkDescriptorPool            descriptorPool;
    VkDescriptorSetLayout       setLayout;
    bool                        freeDescriptorSet   = false;
};
vk::DescriptorSet createDescriptorSet(VkDevice, vk::DescriptorSetCreateInfo const &);

namespace writeDescriptorSet
{

VkWriteDescriptorSet sampler                (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorImageInfo  const>) noexcept;
VkWriteDescriptorSet combinedImageSampler   (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorImageInfo  const>) noexcept;
VkWriteDescriptorSet sampledImage           (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorImageInfo  const>) noexcept;
VkWriteDescriptorSet inputAttachment        (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorImageInfo  const>) noexcept;
VkWriteDescriptorSet storageImage           (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorImageInfo  const>) noexcept;

VkWriteDescriptorSet storageBuffer          (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorBufferInfo const>) noexcept;
VkWriteDescriptorSet uniformBuffer          (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkDescriptorBufferInfo const>) noexcept;

VkWriteDescriptorSet storageTexelBuffer     (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkBufferView           const>) noexcept;
VkWriteDescriptorSet uniformTexelBuffer     (VkDescriptorSet, vk::u32 binding, vk::utils::span<VkBufferView           const>) noexcept;

} // namespace writeDescriptorSet

void updateDescriptorSets
(
    VkDevice,
    vk::utils::span<VkWriteDescriptorSet const>,
    vk::utils::span< VkCopyDescriptorSet const> = {}
);

} // namespace vk
