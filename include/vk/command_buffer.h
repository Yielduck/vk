#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using CommandBuffer = vk::utils::UniqueHandle<VkCommandBuffer>;

struct CommandBufferCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkCommandPool               commandPool;
    VkCommandBufferLevel        level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
};
vk::CommandBuffer createCommandBuffer(VkDevice, vk::CommandBufferCreateInfo const &);

} // namespace vk
