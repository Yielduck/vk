#pragma once
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct InputAssemblyStateCreateInfo
{
    vk::utils::StructureChain               chain                   = {};
    VkPipelineInputAssemblyStateCreateFlags flags                   = {};
    VkPrimitiveTopology                     topology;
    VkBool32                                primitiveRestartEnable  = VK_FALSE;
};

} // namespace vk::pipeline
