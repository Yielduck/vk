#pragma once
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct DepthStencilStateCreateInfo
{
    vk::utils::StructureChain               chain           = {};
    VkPipelineDepthStencilStateCreateFlags  flags           = {};
    VkBool32                                depthTest       = VK_FALSE;
    VkBool32                                depthWrite      = VK_FALSE;
    VkCompareOp                             depthCompare    = VK_COMPARE_OP_LESS_OR_EQUAL;
    VkBool32                                depthBoundsTest = VK_FALSE;
    VkBool32                                stencilTest     = VK_FALSE;
    VkStencilOpState                        front           =
    {
        .failOp       = VK_STENCIL_OP_ZERO,
        .passOp       = VK_STENCIL_OP_KEEP,
        .depthFailOp  = VK_STENCIL_OP_ZERO,
        .compareOp    = VK_COMPARE_OP_LESS_OR_EQUAL,
        .compareMask  = 0,
        .writeMask    = 0,
        .reference    = 0,
    };
    VkStencilOpState                        back            =
    {
        .failOp       = VK_STENCIL_OP_ZERO,
        .passOp       = VK_STENCIL_OP_KEEP,
        .depthFailOp  = VK_STENCIL_OP_ZERO,
        .compareOp    = VK_COMPARE_OP_LESS_OR_EQUAL,
        .compareMask  = 0,
        .writeMask    = 0,
        .reference    = 0,
    };
    vk::f32                                 minDepthBounds  = 0.f;
    vk::f32                                 maxDepthBounds  = 1.f;
};

} // namespace vk::pipeline
