#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct ColorBlendAttachmentState
{
    VkBool32                 blendEnable            = VK_FALSE;
    VkBlendFactor            srcColorBlendFactor    = VK_BLEND_FACTOR_SRC_ALPHA;
    VkBlendFactor            dstColorBlendFactor    = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    VkBlendOp                colorBlendOp           = VK_BLEND_OP_ADD;
    VkBlendFactor            srcAlphaBlendFactor    = VK_BLEND_FACTOR_SRC_ALPHA;
    VkBlendFactor            dstAlphaBlendFactor    = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    VkBlendOp                alphaBlendOp           = VK_BLEND_OP_ADD;
    VkColorComponentFlags    colorWriteMask         = 0xf;
};

struct ColorBlendStateCreateInfo
{
    vk::utils::StructureChain                                       chain               = {};
    VkPipelineColorBlendStateCreateFlags                            flags               = {};
    VkBool32                                                        logicOpEnable       = VK_FALSE;
    VkLogicOp                                                       logicOp             = VK_LOGIC_OP_CLEAR;
    vk::utils::span<vk::pipeline::ColorBlendAttachmentState const>  attachment          = {};
    vk::f32                                                         blendConstants[4]   = {1.f, 1.f, 1.f, 1.f};
};

} // namespace vk::pipeline
