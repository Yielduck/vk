#pragma once
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct TessellationStateCreateInfo
{
    vk::utils::StructureChain               chain               = {};
    VkPipelineTessellationStateCreateFlags  flags               = {};
    vk::u32                                 patchControlPoints  = 1u;
};

} // namespace vk::pipeline
