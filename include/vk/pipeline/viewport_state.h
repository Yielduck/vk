#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct ViewportStateCreateInfo
{
    vk::utils::StructureChain           chain       = {};
    VkPipelineViewportStateCreateFlags  flags       = {};
    vk::utils::span<VkViewport const>   viewport    = {};
    vk::utils::span<VkRect2D   const>   scissor     = {};
};

} // namespace vk::pipeline
