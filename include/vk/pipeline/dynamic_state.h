#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct DynamicStateCreateInfo
{
    vk::utils::StructureChain               chain           = {};
    VkPipelineDynamicStateCreateFlags       flags           = {};
    vk::utils::span<VkDynamicState const>   dynamicState    = {};
};

} // namespace vk::pipeline
