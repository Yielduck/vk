#pragma once
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct RasterizationStateCreateInfo
{
    vk::utils::StructureChain               chain                   = {};
    VkPipelineRasterizationStateCreateFlags flags                   = {};
    VkBool32                                depthClampEnable        = VK_FALSE;
    VkBool32                                rasterizerDiscardEnable = VK_FALSE;
    VkPolygonMode                           polygonMode             = VK_POLYGON_MODE_FILL;
    VkCullModeFlags                         cullMode                = VK_CULL_MODE_NONE;
    VkFrontFace                             frontFace               = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    VkBool32                                depthBiasEnable         = VK_FALSE;
    vk::f32                                 depthBiasConstantFactor = 0.f;
    vk::f32                                 depthBiasClamp          = 0.f;
    vk::f32                                 depthBiasSlopeFactor    = 0.f;
    vk::f32                                 lineWidth               = 1.f;
};

} // namespace vk::pipeline
