#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct ShaderStageCreateInfo
{
    vk::utils::StructureChain                       chain       = {};
    VkPipelineShaderStageCreateFlags                flags       = {};
    VkShaderStageFlagBits                           stage;
    VkShaderModule                                  module;
    char const *                                    name        = "main";
    vk::utils::span<VkSpecializationMapEntry const> mapEntry    = {};
    void const *                                    data        = nullptr;
};

} // namespace vk::pipeline
