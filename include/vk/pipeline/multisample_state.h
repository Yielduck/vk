#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct MultisampleStateCreateInfo
{
    vk::utils::StructureChain               chain                   = {};
    VkPipelineMultisampleStateCreateFlags   flags                   = {};
    VkSampleCountFlagBits                   rasterizationSamples    = VK_SAMPLE_COUNT_1_BIT;
    VkBool32                                sampleShadingEnable     = VK_FALSE;
    vk::f32                                 minSampleShading        = 1.f;
    vk::utils::span<VkSampleMask const>     sampleMask              = {};
    VkBool32                                alphaToCoverageEnable   = VK_FALSE;
    VkBool32                                alphaToOneEnable        = VK_FALSE;
};

} // namespace vk::pipeline
