#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::pipeline
{

struct VertexInputStateCreateInfo
{
    vk::utils::StructureChain                                   chain       = {};
    VkPipelineVertexInputStateCreateFlags                       flags       = {};
    vk::utils::span<VkVertexInputBindingDescription   const>    binding     = {};
    vk::utils::span<VkVertexInputAttributeDescription const>    attribute   = {};
};

} // namespace vk::pipeline
