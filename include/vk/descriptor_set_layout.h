#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using DescriptorSetLayout = vk::utils::UniqueHandle<VkDescriptorSetLayout>;

struct DescriptorSetLayoutCreateInfo
{
    vk::utils::StructureChain                           chain   = {};
    VkDescriptorSetLayoutCreateFlags                    flags   = {};
    vk::utils::span<VkDescriptorSetLayoutBinding const> binding = {};
};
vk::DescriptorSetLayout createDescriptorSetLayout(VkDevice, vk::DescriptorSetLayoutCreateInfo const &);

} // namespace vk
