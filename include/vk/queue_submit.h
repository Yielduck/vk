#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
namespace vk
{

struct SubmitInfo
{
    vk::utils::StructureChain                   chain               = {};
    vk::utils::span<VkSemaphore          const> waitSemaphore       = {};
    vk::utils::span<VkPipelineStageFlags const> waitDstStageMask    = {};
    vk::utils::span<VkCommandBuffer      const> commandBuffer       = {};
    vk::utils::span<VkSemaphore          const> signalSemaphore     = {};
};
void queueSubmit(VkQueue, vk::utils::span<SubmitInfo const>, VkFence = VK_NULL_HANDLE);

} // namespace vk
