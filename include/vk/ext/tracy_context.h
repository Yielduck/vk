#pragma once
#include <vk/utils/proc_addr.h>
#include <vk/utils/unique_handle.h>
#include <vk/ext/tracy.h>

namespace vk::ext
{

using TracyContext = vk::utils::UniqueHandle<TracyVkCtx>;

#ifndef TRACY_ENABLE
inline TracyContext createTracyContext(VkInstance, VkPhysicalDevice, VkDevice, VkQueue, VkCommandBuffer) {return {};}
#else
inline TracyContext createTracyContext( VkInstance const instance
                                      , VkPhysicalDevice const physicalDevice
                                      , VkDevice const device
                                      , VkQueue const queue
                                      , VkCommandBuffer const commandBuffer
                                      )
{
    return
    {
        tracy::CreateVkContext
        (
            physicalDevice,
            device,
            queue,
            commandBuffer,
            instanceProcAddr(instance, vkGetPhysicalDeviceCalibrateableTimeDomainsEXT),
            deviceProcAddr(device, vkGetCalibratedTimestampsEXT)
        ),
        +[](TracyVkCtx const ctx) noexcept {TracyVkDestroy(ctx);},
    };
}
#endif

} // namespace vk::ext
