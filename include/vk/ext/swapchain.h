#pragma once
#include <vk/command_buffer.h>
#include <vk/command_pool.h>
#include <vk/fence.h>
#include <vk/semaphore.h>
#include <vk/queue_submit.h>

#include <vk/ext/tracy_context.h>

#include <vk/guard/command_buffer.h>

#include <vk/khr/swapchain.h>
#include <vk/khr/queue_present.h>

namespace vk::ext
{

class Swapchain
{
public:
    struct CreateInfo
    {
        VkInstance instance;
        VkPhysicalDevice physicalDevice;
        VkDevice device;

        VkSurfaceKHR surface;
        VkSurfaceFormatKHR surfaceFormat;

        vk::u32 queueFamily;
        VkQueue queue;

        vk::u32 minImageCount;
    };
    Swapchain(CreateInfo const &);

    struct Frame
    {
        VkDevice        device;
        VkQueue         queue;
        VkSwapchainKHR  swapchain;

        VkExtent2D                      extent;
        vk::utils::span<VkImage const>  image;

        VkSemaphore     signalSemaphore;
        VkSemaphore     waitSemaphore;
        VkFence         inFlightFence;
        VkCommandBuffer commandBuffer;
        TracyVkCtx      tracyContext;

        struct value_t
        {
            TracyVkCtx      tracyContext;
            VkCommandBuffer commandBuffer;
            vk::u32 imageIndex;

            VkExtent2D extent;
            vk::utils::span<VkImage const> image;
        };
        value_t acquire(               ) const;
        void    release(value_t const &) const;
    };
    vk::utils::Guard<Frame> frameGuard(VkExtent2D);

private:
    CreateInfo info;

    VkExtent2D extent;
    vk::u32 frame;
    vk::u32 imageCount;
    vk::CommandPool commandPool;

    vk::khr::Swapchain swapchain;

    vk::utils::array<VkImage> image;

    vk::utils::array<vk::Semaphore    > waitSemaphore;
    vk::utils::array<vk::Semaphore    > signalSemaphore;
    vk::utils::array<vk::Fence        > inFlightFence;
    vk::utils::array<vk::CommandBuffer> commandBuffer;

    vk::utils::array<vk::ext::TracyContext> tracyContext;

    void resize(VkExtent2D);
};

inline Swapchain::Swapchain(Swapchain::CreateInfo const &createInfo)
    : info(createInfo)
    , extent{0u, 0u}
    , frame(0u)
    , imageCount(0u)
    , commandPool(vk::createCommandPool
    (
        info.device,
        {
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
            .queueFamilyIndex = info.queueFamily,
        }
    ))
{}
inline void Swapchain::resize(VkExtent2D const newExtent)
{
    extent = newExtent;
    swapchain = vk::khr::createSwapchain
    (
        info.device,
        {
            .surface = info.surface,
            .minImageCount = info.minImageCount,
            .surfaceFormat = info.surfaceFormat,
            .extent = extent,
            .oldSwapchain = swapchain,
        }
    );
    image = vk::khr::getSwapchainImages(info.device, swapchain);
    imageCount = image.size32();

    if(waitSemaphore.size() == imageCount)
        return;

    waitSemaphore   = vk::utils::array<vk::Semaphore        >(imageCount);
    signalSemaphore = vk::utils::array<vk::Semaphore        >(imageCount);
    inFlightFence   = vk::utils::array<vk::Fence            >(imageCount);
    commandBuffer   = vk::utils::array<vk::CommandBuffer    >(imageCount);
    tracyContext    = vk::utils::array<vk::ext::TracyContext>(imageCount);
    for(vk::u32 i = 0u; i < imageCount; ++i)
    {
        waitSemaphore  [i] = vk::createSemaphore(info.device);
        signalSemaphore[i] = vk::createSemaphore(info.device);
        inFlightFence  [i] = vk::createFence(info.device, {.flags = VK_FENCE_CREATE_SIGNALED_BIT});
        commandBuffer  [i] = vk::createCommandBuffer(info.device, {.commandPool = commandPool});

        tracyContext   [i] = vk::ext::createTracyContext
        (
            info.instance,
            info.physicalDevice,
            info.device,
            info.queue,
            commandBuffer[i]
        );
    }
}
inline vk::utils::Guard<Swapchain::Frame> Swapchain::frameGuard(VkExtent2D const e)
{
    if(extent.width != e.width || extent.height != e.height)
        resize(e);

    vk::utils::Guard<Swapchain::Frame> guard
    ({
        .device             = info.device,
        .queue              = info.queue,
        .swapchain          = swapchain,
        .extent             = extent,
        .image              = image,
        .signalSemaphore    = signalSemaphore[frame],
        .waitSemaphore      =   waitSemaphore[frame],
        .inFlightFence      =   inFlightFence[frame],
        .commandBuffer      =   commandBuffer[frame],
        .tracyContext       =    tracyContext[frame],
    });
    frame = (frame + 1) % imageCount;
    return guard;
}
inline typename Swapchain::Frame::value_t Swapchain::Frame::acquire() const
{
    {
        ZoneScopedNC("Swapchain: vkQueueSubmit fence wait", 0xA03030);
        FrameMark;
        vk::waitForFences(device, {{inFlightFence}});
    }

    vk::u32 imageIndex = 0u;
    vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, waitSemaphore, VK_NULL_HANDLE, &imageIndex);

    return
    {
        .tracyContext   = tracyContext,
        .commandBuffer  = commandBuffer,
        .imageIndex     = imageIndex,
        .extent         = extent,
        .image          = image,
    };
}
inline void Swapchain::Frame::release(value_t const &value) const
{
    vk::resetFences(device, {{inFlightFence}});

    {
        ZoneScopedNC("Swapchain: vkAcquireNextImage semaphore wait", 0x30A030);
        vk::queueSubmit
        (
            queue,
            {{
                {
                    .waitSemaphore      = {{waitSemaphore}},
                    .waitDstStageMask   = {{VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT}},
                    .commandBuffer      = {{value.commandBuffer}},
                    .signalSemaphore    = {{signalSemaphore}},
                }
            }},
            inFlightFence
        );
    }
    vk::khr::queuePresent
    (
        queue,
        {
            .waitSemaphore  = {{signalSemaphore}},
            .swapchain      = swapchain,
            .imageIndex     = value.imageIndex,
        }
    );
}

} // namespace vk::ext
