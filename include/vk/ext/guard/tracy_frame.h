#pragma once
#include <vk/utils/guard.h>
#include <vk/ext/tracy.h>

namespace vk::ext::guard
{

struct TracyFrameState
{
    char const *name;

    using value_t = char const *;
    value_t acquire(               ) const {FrameMarkStart(name); return name;}
    void    release(value_t const &) const {FrameMarkEnd  (name);}
};
using TracyFrame = vk::utils::Guard<TracyFrameState>;

} // namespace vk::ext::guard
