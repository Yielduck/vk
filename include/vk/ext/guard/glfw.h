#pragma once
#include <vk/utils/guard.h>
#include <vk/utils/span.h>
#include <GLFW/glfw3.h>
namespace vk::ext::guard
{

struct GLFWState
{
    struct Hint
    {
        int hint;
        int value;
    };
    vk::utils::span<Hint const> initHint = {};
    GLFWerrorfun errorCallback = nullptr;

    struct value_t
    {
        int version[3]; // major, minor, revision
        char const *versionString;
        vk::utils::span<char const * const> requiredExtensions;
    };
    value_t acquire() const
    {
        if(errorCallback != nullptr)
            glfwSetErrorCallback(errorCallback);

        for(auto const [hint, value] : initHint)
            glfwInitHint(hint, value);

        if(GLFW_TRUE != glfwInit())
            throw std::runtime_error("glfwInit: false");
        if(GLFW_TRUE != glfwVulkanSupported())
            throw std::runtime_error("glfwVulkanSupported: false");

        int ver[3];
        glfwGetVersion(ver, ver + 1, ver + 2);

        vk::u32 extCount;
        char const ** const ext = glfwGetRequiredInstanceExtensions(&extCount);
        return
        {
            .version = {ver[0], ver[1], ver[2]},
            .versionString = glfwGetVersionString(),
            .requiredExtensions = {ext, extCount},
        };
    }
    void   release(value_t const &) const
    {
        glfwTerminate();
    }
};

using GLFW = vk::utils::Guard<GLFWState>;

} // namespace vk::ext::guard
