#pragma once
#include <vk/guard/command_buffer.h>
#include <vk/ext/tracy.h>

namespace vk::ext::guard
{

struct TracyCommandBufferState
{
    TracyVkCtx                      tracyContext;
    vk::guard::CommandBufferState   commandBufferState;

    struct value_t
    {
        TracyVkCtx      tracyContext;
        VkCommandBuffer commandBuffer;
    };

    value_t acquire() const
    {
        return
        {
            tracyContext,
            commandBufferState.acquire(),
        };
    }
    void release(value_t const &value) const
    {
        TracyVkCollect(value.tracyContext, value.commandBuffer);
        commandBufferState.release(value.commandBuffer);
    }
};
using TracyCommandBuffer = vk::utils::Guard<TracyCommandBufferState>;

} // namespace vk::ext::guard
