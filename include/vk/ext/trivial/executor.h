#pragma once
#include <vk/command_pool.h>
#include <vk/command_buffer.h>
#include <vk/fence.h>
#include <vk/guard/command_buffer.h>
#include <vk/queue_submit.h>
namespace vk::ext::trivial
{

struct Executor
{
    struct CreateInfo
    {
        VkDevice device;

        vk::u32 queueFamily;
        VkQueue queue;
    } info;

    vk::Fence fence;

    vk::CommandPool commandPool;
    vk::CommandBuffer commandBuffer;

    Executor(CreateInfo const &createInfo)
        : info(createInfo)
        , fence(vk::createFence(info.device))
        , commandPool(vk::createCommandPool(info.device, {.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, .queueFamilyIndex = info.queueFamily}))
        , commandBuffer(vk::createCommandBuffer(info.device, {.commandPool = commandPool}))
    {}

    template<typename F>
    vk::utils::UniqueHandle<VkFence> execute(F const &commands) const // returns a fence waiter
    {
        {
            vk::guard::CommandBuffer const guard({.commandBuffer = commandBuffer});
            commands(commandBuffer);
        }
        vk::queueSubmit
        (
            info.queue,
            {{ {.commandBuffer = {{commandBuffer}}, } }},
            fence
        );
        return
        {
            fence,
            [device = info.device](VkFence const handle) noexcept
            {
                vk::waitForFences(device, {{handle}});
                vk::resetFences(device, {{handle}});
            },
        };
    }
};

} // namespace vk::ext::trivial
