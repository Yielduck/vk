#pragma once
#include <vk/instance.h>
#include <vk/ext/find.h>
#include <vector>

namespace vk::ext::trivial
{

vk::Instance Instance(vk::InstanceCreateInfo info)
{
    auto const props = vk::enumerateInstanceExtensionProperties();
    auto const supports = vk::ext::find::extensionName(props);

    std::vector<char const *> instanceE;
    for(char const * const ext : info.extension)
    {
        if(supports(ext))
            instanceE.push_back(ext);
        else
            throw std::runtime_error("instance extension \"" + std::string(ext) + "\" is not supported");
    }

#ifdef VK_KHR_portability_enumeration
    char const portabilityExt[] = VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME;
    bool const portability = supports(portabilityExt);
    if(portability)
        instanceE.push_back(portabilityExt);
#endif

    return vk::createInstance
    ({
        .chain              = std::move(info.chain),
#ifdef VK_KHR_portability_enumeration
        .flags              = portability
            ? VkInstanceCreateFlags(VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR)
            : VkInstanceCreateFlags(0),
#else
        .flags              = {},
#endif
        .applicationName    = info.applicationName,
        .applicationVersion = info.applicationVersion,
        .engineName         = info.engineName,
        .engineVersion      = info.engineVersion,
        .apiVersion         = info.apiVersion,
        .validationLayer    = info.validationLayer,
        .extension          = instanceE,
    });
}


} // namespace vk::ext::trivial
