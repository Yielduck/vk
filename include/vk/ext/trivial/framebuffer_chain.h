#pragma once
#include <vk/image_view.h>
#include <vk/framebuffer.h>
#include <vector>
namespace vk::ext::trivial
{

struct FramebufferChain
{
    VkDevice device;
    VkRenderPass renderPass;

    VkFormat format;

    std::vector<vk::ImageView> imageView = {};
    std::vector<vk::Framebuffer> framebuffer = {};

    VkExtent2D extent = {0, 0};
    
    void update(VkExtent2D const e, vk::utils::span<VkImage const> const swapchainImage) noexcept
    {
        if(extent.width == e.width && extent.height == e.height)
            return;

        extent = e;
        imageView.clear();
        framebuffer.clear();

        for(VkImage const image : swapchainImage)
        {
            imageView.push_back(vk::createImageView
            (
                device,
                {
                    .image = image,
                    .type = VK_IMAGE_VIEW_TYPE_2D,
                    .format = format,
                }
            ));

            framebuffer.push_back(vk::createFramebuffer
            (
                device,
                {
                    .renderPass = renderPass,
                    .attachment = {{imageView.back()}},
                    .width  = extent.width,
                    .height = extent.height,
                }
            ));
        }
    }
};

} // namespace vk::ext::trivial
