#pragma once
#include <vulkan/vulkan_beta.h> // VK_KHR_portability_subset

#include <vk/device.h>
#include <vk/instance.h>
#include <vk/physical_device_info.h>

#include <vk/ext/find.h>

#include <vector>
#include <string>
#include <stdexcept>

namespace vk::ext::trivial
{

struct Device
{
    vk::Instance instance;
    VkPhysicalDevice physicalDevice;
    vk::PhysicalDeviceInfo deviceInfo;
    vk::Device device;

    vk::u32  genericQueueFamily; // supports both compute and graphics
    vk::u32  computeQueueFamily; // supports compute
    vk::u32 graphicsQueueFamily; // supports graphics

    enum class QueueFamilyRelations : vk::u32
    {
        AllDistinct             = 0b00u,
        ComputeEqualsGeneric    = 0b01u,
        GraphicsEqualsGeneric   = 0b10u,
        AllGeneric              = 0b11u,
    } queueFamilyRelations;
    /*
     * there may be less than three queue families on the device
     * in this case, computeQueueFamily is equal to genericQueueFamily
     *           or graphicsQueueFamily is equal to genericQueueFamily
     *           or, in case there is only one queue family, all three are equal
     */

    VkQueue  genericQueue;
    VkQueue  computeQueue;
    VkQueue graphicsQueue;
    /*
     * queues may overlap in case queueCount of genericQueueFamily is too low
     * like when there's only one queue family with queueCount = 1
     */

    struct CreateInfo
    {
        vk::Instance instance;
        vk::utils::StructureChain chain = {};
        vk::utils::StructureChain features = {};
        // vk::getPhysicalDeviceFeatures2(features) is appended to a chain
        // VkPhysicalDeviceFeatures are automatically enabled
        vk::utils::span<char const * const> requiredExtensions = {};
        vk::utils::span<char const * const> optionalExtensions = {};
    };

    Device(CreateInfo);
};

inline Device::Device(Device::CreateInfo info)
    : instance(static_cast<vk::Instance &&>(info.instance))
{
    auto const physicalDevices = vk::enumeratePhysicalDevices(instance);
    if(physicalDevices.empty())
        throw std::runtime_error("no physical device found");
    auto const physicalDevicesInfo = physicalDevices | std::views::transform(vk::physicalDeviceInfo);

    auto const discreteGPUIter = std::ranges::find
    (
        physicalDevicesInfo,
        VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
        +[](vk::PhysicalDeviceInfo const &i) noexcept {return i.properties.deviceType;}
    );
    vk::u32 const deviceIndex = discreteGPUIter == std::ranges::end(physicalDevicesInfo)
        ? 0
        : vk::u32(discreteGPUIter - std::ranges::begin(physicalDevicesInfo));

    deviceInfo = physicalDevicesInfo[deviceIndex];
    physicalDevice = physicalDevices[deviceIndex];

    auto const supportsDeviceExtension = vk::ext::find::extensionName(deviceInfo.extensionProperties);

    std::vector<char const *> deviceE;
    for(char const * const ext : info.requiredExtensions)
    {
        if(supportsDeviceExtension(ext))
            deviceE.push_back(ext);
        else
            throw std::runtime_error("device extension \"" + std::string(ext) + "\" is not supported");
    }
    for(char const * const ext : info.optionalExtensions)
        if(supportsDeviceExtension(ext))
            deviceE.push_back(ext);

#ifdef VK_KHR_portability_subset
    char const portabilitySubset[] = VK_KHR_PORTABILITY_SUBSET_EXTENSION_NAME;
    if(supportsDeviceExtension(portabilitySubset))
        deviceE.push_back(portabilitySubset);
#endif

    auto const findQueueFamily = vk::ext::find::queueFamily(deviceInfo.queueProperties);
    auto  genericQueues = findQueueFamily(vk::ext::predicate::supportsGC);
    auto  computeQueues = findQueueFamily(vk::ext::predicate::supportsComputeNotGraphics);
    auto graphicsQueues = findQueueFamily(vk::ext::predicate::supportsGraphicsNotCompute);

    if(std::ranges::empty(genericQueues))
        throw std::runtime_error("no compute+graphics queue family found");

    genericQueueFamily = genericQueues.front();

    auto generic1 = genericQueues | std::views::drop(1);
    auto generic2 = genericQueues | std::views::drop(2);

    bool const hasCompute  = !std::ranges::empty( computeQueues);
    bool const hasGraphics = !std::ranges::empty(graphicsQueues);

    bool const has2Generics = !std::ranges::empty(generic1);
    bool const has3Generics = !std::ranges::empty(generic2);

    /*
     * case 0:  hasCompute &&  hasGraphics                                  -> AllDistinct
     *
     * case 1: !hasCompute &&  hasGraphics &&  has2Generics                 -> AllDistinct
     * case 2: !hasCompute &&  hasGraphics && !has2Generics                 ->  ComputeEqualsGeneric
     *
     * case 3:  hasCompute && !hasGraphics &&  has2Generics                 -> AllDistinct
     * case 4:  hasCompute && !hasGraphics && !has2Generics                 -> GraphicsEqualsGeneric
     *
     * case 5: !hasCompute && !hasGraphics &&  has3Generics                 -> AllDistinct
     * case 6: !hasCompute && !hasGraphics && !has3Generics && has2Generics -> GraphicsEqualsGeneric
     * case 7: !hasCompute && !hasGraphics && !has2Generics                 -> AllGeneric
     */

    vk::u32 const state = hasGraphics
        ? (hasCompute ? 0u : (has2Generics ? 1u : 2u))
        : (hasCompute ?      (has2Generics ? 3u : 4u) : (has3Generics ? 5u : (has2Generics ? 6u : 7u)));

    queueFamilyRelations = state == 7u
        ? QueueFamilyRelations::AllGeneric
        : ((state & 0x1) == 1u
            ? QueueFamilyRelations::AllDistinct
            : (state == 2u
                ? QueueFamilyRelations:: ComputeEqualsGeneric
                : QueueFamilyRelations::GraphicsEqualsGeneric
              )
          );
    vk::u32 const rel = static_cast<vk::u32>(queueFamilyRelations);
    vk::u32 const relBitCount = rel == 0u
        ? 0u
        : (rel == 3u ? 2u : 1u);

    computeQueueFamily = hasCompute
         ? computeQueues.front()
         : (has2Generics
            ? generic1.front()
            : genericQueueFamily
         );

    graphicsQueueFamily = hasGraphics
        ? graphicsQueues.front()
        : (hasCompute
            ? (has2Generics ? generic1.front() : genericQueueFamily)
            : (has3Generics ? generic2.front() : genericQueueFamily)
        );

    vk::f32 const priority[3] = {1.f, 1.f, 1.f};
    vk::DeviceCreateInfo::QueueInfo queueInfo[3];
    vk::u32 const genericQueueMaxCount = deviceInfo.queueProperties[genericQueueFamily].queueCount;

    queueInfo[0] =
    {
        .queueFamilyIndex = genericQueueFamily,
        .priority = {priority, std::min(genericQueueMaxCount, relBitCount + 1u)},
    };
    queueInfo[1] =
    {
        .queueFamilyIndex = state == 2u
            ? graphicsQueueFamily
            :  computeQueueFamily,
        .priority = {priority, 1u},
    };
    queueInfo[2] =
    {
        .queueFamilyIndex = graphicsQueueFamily,
        .priority = {priority, 1u},
    };

    auto features = vk::getPhysicalDeviceFeatures2(physicalDevice, std::move(info.features));
    device = vk::createDevice
    (
        physicalDevice,
        {
            .chain = static_cast<vk::utils::StructureChain &&>(info.chain.append(features)),
            .queueInfo = {queueInfo, 3u - relBitCount},
            .extension = deviceE,
            .enabledFeatures = &deviceInfo.features,
        }
    );

    vk::u32 const  computeQueueIdx[8] = {0u, 0u, 1u, 0u, 0u, 0u, 0u, 1u};
    vk::u32 const graphicsQueueIdx[8] = {0u, 0u, 0u, 0u, 1u, 0u, 1u, 2u};
     genericQueue = vk::getDeviceQueue(device,  genericQueueFamily, 0u);
     computeQueue = vk::getDeviceQueue(device,  computeQueueFamily, std::min(genericQueueMaxCount - 1u,  computeQueueIdx[state]));
    graphicsQueue = vk::getDeviceQueue(device, graphicsQueueFamily, std::min(genericQueueMaxCount - 1u, graphicsQueueIdx[state]));
};

} // namespace vk::ext::trivial
