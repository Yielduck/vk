#pragma once
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>
#include <vk/guard/command_buffer.h>

namespace vk::ext::imgui
{

struct Frame
{
    ImGuiContext *context;
    VkCommandBuffer commandBuffer;

    using value_t = VkCommandBuffer;
    value_t acquire() const
    {
        ImGui::SetCurrentContext(context);

        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        return commandBuffer;
    }
    void release(value_t const buffer) const
    {
        ImGui::Render();
        ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), buffer);
    }
};
using FrameGuard = vk::utils::Guard<Frame>;

} // namespace vk::ext::imgui
