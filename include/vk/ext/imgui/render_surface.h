#pragma once
#include <vk/descriptor_pool.h>
#include <vk/render_pass.h>
#include <vk/guard/render_pass.h>
#include <vk/ext/swapchain.h>
#include <vk/ext/imgui/context.h>
#include <vk/ext/trivial/framebuffer_chain.h>

#include <GLFW/glfw3.h>

#include <stdexcept>

namespace vk::ext::imgui
{

class RenderSurface
{
public:
    struct CreateInfo
    {
        VkInstance instance;
        VkPhysicalDevice physicalDevice;
        VkDevice device;

        vk::u32 queueFamily;
        VkQueue queue;

        GLFWwindow *window;
        VkSurfaceKHR surface;
        VkSurfaceFormatKHR surfaceFormat;

        vk::u32 minImageCount = 2u;
        VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT;
    };

    RenderSurface(CreateInfo);

    vk::utils::Guard<vk::ext::imgui::Context> imguiContextGuard(vk::utils::span<ImFontConfig const> const font = {}) const;

    using Frame = typename vk::ext::Swapchain::Frame;
    vk::utils::Guard<Frame> frameGuard();

    vk::guard::RenderPass renderPassGuard(vk::utils::Guard<Frame> const &);

    VkRenderPass renderPassHandle() const noexcept {return renderPass;}

private:
    CreateInfo info;
    vk::DescriptorPool descriptorPool;

    vk::ext::Swapchain swapchain;

    vk::RenderPass renderPass;
    vk::ext::trivial::FramebufferChain chain;
};

inline RenderSurface::RenderSurface(CreateInfo const createInfo)
    : info(createInfo)
    , descriptorPool(vk::createDescriptorPool
    (
        info.device,
        {
            .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
            .maxSets = 64,
            .poolSize =
            {{
                {VK_DESCRIPTOR_TYPE_SAMPLER               , 64},
                {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 64},
                {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE         , 64},
                {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE         , 64},
                {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER  , 64},
                {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER  , 64},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER        , 64},
                {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER        , 64},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 64},
                {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 64},
                {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT      , 64},
            }},
        }
    ))
    , swapchain
    ({
        .instance = info.instance,
        .physicalDevice = info.physicalDevice,
        .device = info.device,
        .surface = info.surface,
        .surfaceFormat = info.surfaceFormat,
        .queueFamily = info.queueFamily,
        .queue = info.queue,
        .minImageCount = info.minImageCount,
    })
    , renderPass(vk::createRenderPass
    (
        info.device,
        {
            .attachment =
            {{ {
                .flags          = {},
                .format         = info.surfaceFormat.format,
                .samples        = info.sampleCount,
                .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
                .storeOp        = VK_ATTACHMENT_STORE_OP_STORE,
                .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
                .finalLayout    = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            }} },
            .subpass =
            {{ {
                .colorAttachment =
                {{ {
                      .attachment = 0u,
                      .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                }} }
            }} },
            .dependency =
            {{ {
                .srcSubpass = VK_SUBPASS_EXTERNAL,
                .dstSubpass = 0u,
                .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .srcAccessMask = 0,
                .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                .dependencyFlags = {},
            }} }
        }
    ))
    , chain
    {
        .device = info.device,
        .renderPass = renderPass,
        .format = info.surfaceFormat.format,
    }
{}

inline vk::utils::Guard<vk::ext::imgui::Context> RenderSurface::imguiContextGuard(vk::utils::span<ImFontConfig const> const font) const
{
    return
    {{
        .window = info.window,
        .info =
        {
            .Instance       = info.instance,
            .PhysicalDevice = info.physicalDevice,
            .Device         = info.device,
            .QueueFamily    = info.queueFamily,
            .Queue          = info.queue,
            .DescriptorPool = descriptorPool,
            .RenderPass     = renderPass,
            .MinImageCount  = info.minImageCount,
            .ImageCount     = info.minImageCount,
            .MSAASamples    = info.sampleCount,

            .PipelineCache  = VK_NULL_HANDLE,
            .Subpass        = 0u,

            .UseDynamicRendering = false,
#ifdef IMGUI_IMPL_VULKAN_HAS_DYNAMIC_RENDERING
            .PipelineRenderingCreateInfo = {},
#endif

            .Allocator = pAlloc,
            .CheckVkResultFn = +[](VkResult const vkresult)
            {
                if(VK_SUCCESS != vkresult)
                    throw std::runtime_error("imgui: something went wrong");
            },
            .MinAllocationSize = 1,
        },
        .font = font,
    }};
}

inline vk::utils::Guard<typename Swapchain::Frame> RenderSurface::frameGuard()
{
    int width, height;
    glfwGetFramebufferSize(info.window, &width, &height);
    return swapchain.frameGuard({vk::u32(width), vk::u32(height)});
}
inline vk::guard::RenderPass RenderSurface::renderPassGuard(vk::utils::Guard<Frame> const &frame)
{
    auto const [_, commandBuffer, imageIndex, extent, image] = frame.cref();
    chain.update(extent, image);

    auto const [width, height] = extent;
    VkRect2D const renderArea =
    {
        {0, 0},
        {width, height},
    };
    VkViewport const viewport =
    {
        .x        = 0.f,
        .y        = 0.f,
        .width    = static_cast<vk::f32>(width),
        .height   = static_cast<vk::f32>(height),
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    vk::guard::RenderPass guard
    ({
        .commandBuffer  = commandBuffer,
        .renderPass     = renderPass,
        .framebuffer    = chain.framebuffer[imageIndex],
        .renderArea     = renderArea,
        .clearValue     = {{ {{{0.f, 0.f, 0.f, 1.f}}} }},
    });

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor (commandBuffer, 0, 1, &renderArea);

    return guard;
}

} // namespace vk::ext::imgui
