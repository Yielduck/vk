#pragma once
#include <imgui_impl_glfw.h>
#include <imgui_impl_vulkan.h>

#include <vk/guard/command_buffer.h>
#include <vk/command_buffer.h>
#include <vk/command_pool.h>
#include <vk/queue_submit.h>

#include <GLFW/glfw3.h>

namespace vk::ext::imgui
{

struct Context
{
    GLFWwindow *window;
    ImGui_ImplVulkan_InitInfo info;
    vk::utils::span<ImFontConfig const> font;

    struct value_t
    {
        ImGuiContext *handle;
        GLFWwindow   *window;
    };

    value_t acquire() const
    {
        IMGUI_CHECKVERSION();
        ImGuiContext * const context = ImGui::CreateContext();

        ImGui_ImplGlfw_InitForVulkan(window, false);
        ImGui_ImplVulkan_Init(const_cast<ImGui_ImplVulkan_InitInfo *>(&info)); // thank you imgui for your const-correctness

        {
            float xscale, yscale;
            glfwGetWindowContentScale(window, &xscale, &yscale);
            float const fontScale = xscale > yscale ? xscale : yscale;

            ImGuiIO &io = ImGui::GetIO();
            io.FontGlobalScale = 1.f / fontScale;
            for(ImFontConfig config : font)
            {
                config.SizePixels *= fontScale;
                io.Fonts->AddFont(&config);
            }
            ImGui_ImplVulkan_CreateFontsTexture();
        }

        return {context, window};
    }
    void release(value_t const v) const
    {
        ImGui_ImplVulkan_DestroyFontsTexture();
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext(v.handle);
    }
};
using ContextGuard = vk::utils::Guard<Context>;

} // namespace vk::ext::imgui
