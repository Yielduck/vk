#pragma once
#include <vk/utils/array.h>
#include <vk/utils/unique_handle.h>
#include <GLFW/glfw3.h>
namespace vk::ext
{

struct GLFWSurface
{
    vk::utils::UniqueHandle<GLFWwindow *> window;
    vk::utils::UniqueHandle<VkSurfaceKHR> surface;

    GLFWSurface(VkInstance, GLFWmonitor * = glfwGetPrimaryMonitor());

    vk::utils::array<VkSurfaceFormatKHR> availableFormat(VkPhysicalDevice) const;
    VkSurfaceCapabilitiesKHR                capabilities(VkPhysicalDevice) const;
};

inline GLFWSurface::GLFWSurface(VkInstance const instance, GLFWmonitor * const monitor)
{
    {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

        GLFWvidmode const * const mode = glfwGetVideoMode(monitor);
        GLFWwindow * const pWindow = glfwCreateWindow(mode->width, mode->height, "The Window", nullptr, nullptr);
        if(nullptr == pWindow)
            throw vk::Exception{VK_ERROR_INITIALIZATION_FAILED, "glfwCreateWindow"};

        window = vk::utils::UniqueHandle<GLFWwindow *>(pWindow, glfwDestroyWindow);
    }
    {
        VkSurfaceKHR surfaceKHR = VK_NULL_HANDLE;
        if(VK_SUCCESS != glfwCreateWindowSurface(instance, window, pAlloc, &surfaceKHR))
            throw vk::Exception{VK_ERROR_INITIALIZATION_FAILED, "glfwCreateWindowSurface"};

        surface = vk::utils::UniqueHandle<VkSurfaceKHR>
        (
            surfaceKHR,
            [instance](VkSurfaceKHR const handle) noexcept
            {
                vkDestroySurfaceKHR(instance, handle, pAlloc);
            }
        );
    }
}
vk::utils::array<VkSurfaceFormatKHR> GLFWSurface::availableFormat(VkPhysicalDevice const device) const
{
    vk::u32 formatCount = 0u;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
    vk::utils::array<VkSurfaceFormatKHR> fmt(formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, fmt.data());
    return fmt;
}
VkSurfaceCapabilitiesKHR GLFWSurface::capabilities(VkPhysicalDevice const device) const
{
    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &capabilities);
    return capabilities;
}

} // namespace vk::ext
