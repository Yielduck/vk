#pragma once
#include "../utils/span.h"
#include <ranges>
#include <cstring>

namespace vk::ext::predicate
{

constexpr auto     equals(VkQueueFlagBits const flags) noexcept
{
    return [f = VkQueueFlags(flags)](VkQueueFamilyProperties const props) noexcept {return f == props.queueFlags;};
}
constexpr auto intersects(VkQueueFlagBits const flags) noexcept
{
    return [f = VkQueueFlags(flags)](VkQueueFamilyProperties const props) noexcept {return 0u != (f & props.queueFlags);};
}

constexpr auto     equals(VkMemoryPropertyFlagBits const flags) noexcept
{
    return [f = VkMemoryPropertyFlags(flags)](vk::PhysicalDeviceMemoryProperties const props) noexcept {return f == props.type.propertyFlags;};
}
constexpr auto intersects(VkMemoryPropertyFlagBits const flags) noexcept
{
    return [f = VkMemoryPropertyFlags(flags)](vk::PhysicalDeviceMemoryProperties const props) noexcept {return 0u != (f & props.type.propertyFlags);};
}

template<typename Predicate>
constexpr auto negation(Predicate p) noexcept
{
    return [=](auto const props) noexcept {return !p(props);};
}
template<typename... Predicate>
constexpr auto conjunction(Predicate ... p) noexcept
{
    return [=](auto const props) noexcept {return (p(props) && ...);};
}
template<typename... Predicate>
constexpr auto disjunction(Predicate ... p) noexcept
{
    return [=](auto const props) noexcept {return (p(props) || ...);};
}

inline constexpr auto deviceLocal = intersects(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
inline constexpr auto hostVisible = intersects(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

inline constexpr auto supportsGraphics = intersects(VK_QUEUE_GRAPHICS_BIT);
inline constexpr auto supportsCompute  = intersects(VK_QUEUE_COMPUTE_BIT);
inline constexpr auto supportsGC = conjunction(supportsGraphics, supportsCompute);
inline constexpr auto supportsComputeNotGraphics = conjunction(supportsCompute, negation(supportsGraphics));
inline constexpr auto supportsGraphicsNotCompute = conjunction(supportsGraphics, negation(supportsCompute));

} // namespace vk::ext::predicate

namespace vk::ext::find
{

inline auto extensionName(vk::utils::span<VkExtensionProperties const> const props) noexcept
{
    return [props](char const * const name) noexcept
    {
        return std::ranges::end(props) != std::ranges::find_if
        (
            props,
            [name](char const * const other) noexcept {return std::strcmp(name, other) == 0;},
            &VkExtensionProperties::extensionName
        );
    };
}

inline auto queueFamily(vk::utils::span<VkQueueFamilyProperties const> const props) noexcept
{
    return [props]<typename Predicate>(Predicate &&pred) noexcept
    {
        return std::views::iota(0u, props.size32()) | std::views::filter
        (
            [props, predicate = static_cast<Predicate &&>(pred)](vk::u32 const i) noexcept
            {
                return predicate(props[i]);
            }
        );
    };
}
inline auto memoryIndex(vk::utils::span<vk::PhysicalDeviceMemoryProperties const> const props) noexcept
{
    return [props]<typename Predicate>(Predicate &&pred, u32 const memoryTypeBits = 0xffffffffu) noexcept
    {
        return std::views::iota(0u, props.size32()) | std::views::filter
        (
            [props, memoryTypeBits, predicate = static_cast<Predicate &&>(pred)](vk::u32 const i) noexcept
            {
                return (((1u << i) & memoryTypeBits) != 0u) && predicate(props[i]);
            }
        );
    };
}

} // namespace vk::ext::find
