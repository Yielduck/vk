#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Semaphore = vk::utils::UniqueHandle<VkSemaphore>;

struct SemaphoreCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkSemaphoreCreateFlags      flags = {};
};

vk::Semaphore createSemaphore(VkDevice, vk::SemaphoreCreateInfo const & = {});

} // namespace vk
