#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using BufferView = vk::utils::UniqueHandle<VkBufferView>;

struct BufferViewCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkBufferViewCreateFlags     flags = {};
    VkBuffer                    buffer;
    VkFormat                    format;
    VkDeviceSize                offset  = 0;
    VkDeviceSize                range   = VK_WHOLE_SIZE;
};
vk::BufferView createBufferView(VkDevice, vk::BufferViewCreateInfo const &);

} // namespace vk
