#pragma once
#include "../utils/array.h"
#include "../utils/span.h"
#include "../utils/structure_chain.h"
#include "../utils/unique_handle.h"
namespace vk::khr
{

#ifdef VK_KHR_swapchain
using Swapchain = vk::utils::UniqueHandle<VkSwapchainKHR>;

// VK_KHR_swapchain
struct SwapchainCreateInfo
{
    vk::utils::StructureChain       chain               = {};
    VkSwapchainCreateFlagsKHR       flags               = {};
    VkSurfaceKHR                    surface;
    vk::u32                         minImageCount;
    VkSurfaceFormatKHR              surfaceFormat;
    VkExtent2D                      extent;
    vk::u32                         imageArrayLayers    = 1;
    VkImageUsageFlags               usage               = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    vk::utils::span<vk::u32 const>  queueFamily         = {};
    VkSurfaceTransformFlagBitsKHR   preTransform        = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    VkCompositeAlphaFlagBitsKHR     compositeAlpha      = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    VkPresentModeKHR                presentMode         = VK_PRESENT_MODE_FIFO_KHR;
    VkBool32                        clipped             = VK_TRUE;
    VkSwapchainKHR                  oldSwapchain        = VK_NULL_HANDLE;
};
vk::khr::Swapchain createSwapchain(VkDevice, vk::khr::SwapchainCreateInfo const &);

vk::utils::array<VkImage> getSwapchainImages(VkDevice, VkSwapchainKHR);
#endif

} // namespace vk::khr
