#pragma once
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::khr
{

#ifdef VK_KHR_swapchain
struct PresentInfo
{
    vk::utils::StructureChain           chain           = {};
    vk::utils::span<VkSemaphore const>  waitSemaphore   = {};
    VkSwapchainKHR                      swapchain;
    vk::u32                             imageIndex;
};
VkResult queuePresent(VkQueue, vk::khr::PresentInfo const &);
#endif

} // namespace vk::khr
