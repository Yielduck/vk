#pragma once
#include "../vk.h"

namespace vk::khr
{

#ifdef VK_KHR_buffer_device_address
VkDeviceAddress getBufferDeviceAddress(VkDevice, VkBuffer) noexcept;
vk::u64  getBufferOpaqueCaptureAddress(VkDevice, VkBuffer) noexcept;
#endif

} // namespace vk::khr

namespace vk
{

#ifdef VK_API_VERSION_1_2
VkDeviceAddress getBufferDeviceAddress(VkDevice, VkBuffer) noexcept;
vk::u64  getBufferOpaqueCaptureAddress(VkDevice, VkBuffer) noexcept;
#endif

} // namespace vk
