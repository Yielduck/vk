#pragma once
#include "../utils/array.h"
namespace vk::khr
{

#ifdef VK_KHR_pipeline_executable_properties
struct PipelineExecutableInfo
{
    VkPipelineExecutablePropertiesKHR properties;
    vk::utils::array<VkPipelineExecutableStatisticKHR> statistics;
};
vk::utils::array<vk::khr::PipelineExecutableInfo> pipelineExecutableInfo(VkDevice, VkPipeline);
#endif

} // namespace vk::khr
