#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using CommandPool = vk::utils::UniqueHandle<VkCommandPool>;

struct CommandPoolCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkCommandPoolCreateFlags    flags = {};
    vk::u32                     queueFamilyIndex;
};
vk::CommandPool createCommandPool(VkDevice, vk::CommandPoolCreateInfo const &);

} // namespace vk
