#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using RenderPass = vk::utils::UniqueHandle<VkRenderPass>;

struct SubpassDescription
{
    VkSubpassDescriptionFlags                       flags                   = {};
    VkPipelineBindPoint                             bindPoint               = VK_PIPELINE_BIND_POINT_GRAPHICS;
    vk::utils::span<VkAttachmentReference const>    inputAttachment         = {};
    vk::utils::span<VkAttachmentReference const>    colorAttachment         = {};
    vk::utils::span<VkAttachmentReference const>    resolveAttachment       = {};
    VkAttachmentReference                           depthStencilAttachment  =
    {
        .attachment = VK_ATTACHMENT_UNUSED,
        .layout     = VK_IMAGE_LAYOUT_UNDEFINED,
    };
    vk::utils::span<vk::u32 const>                  preserveAttachment      = {};
};
struct RenderPassCreateInfo
{
    vk::utils::StructureChain                       chain       = {};
    VkRenderPassCreateFlags                         flags       = {};
    vk::utils::span<VkAttachmentDescription const>  attachment;
    vk::utils::span<SubpassDescription      const>  subpass;
    vk::utils::span<VkSubpassDependency     const>  dependency  = {};
};
vk::RenderPass createRenderPass(VkDevice, vk::RenderPassCreateInfo const &);

} // namespace vk
