#pragma once
#include "erased_ptr.h"
#include "type_traits.h"

namespace vk::utils
{

template<typename T>
class Function;

namespace detail
{
template<typename>
struct function_guide_helper {};

template<typename R, typename F, bool N, typename... Args>
struct function_guide_helper<R (F::*) (Args...)         noexcept(N)> { using type = R(Args...); };

template<typename R, typename F, bool N, typename... Args>
struct function_guide_helper<R (F::*) (Args...)       & noexcept(N)> { using type = R(Args...); };

template<typename R, typename F, bool N, typename... Args>
struct function_guide_helper<R (F::*) (Args...) const   noexcept(N)> { using type = R(Args...); };

template<typename R, typename F, bool N, typename... Args>
struct function_guide_helper<R (F::*) (Args...) const & noexcept(N)> { using type = R(Args...); };

template<typename R, typename F, bool N, typename... Args>
struct function_guide_helper<R (*) (F, Args...) noexcept(N)> { using type = R(Args...); };

template<typename F>
using function_guide_t = typename function_guide_helper<decltype(&F::operator())>::type;

} // namespace detail

template<typename R, typename... Args>
Function(R(*)(Args...)) -> Function<R(Args...)>;

template<typename F>
Function(F) -> Function<detail::function_guide_t<F>>;

template<typename R, typename... Args>
class Function<R(Args...)>
{
    erased_ptr state = {};
    R(*invoke)(erased_ptr const &, Args &&...) = nullptr;

public:
    constexpr Function() noexcept = default;

    template<typename F>
        requires(!vk::utils::is_same_v<vk::utils::remove_cvref_t<F>, Function<R(Args...)>>)
    Function(F &&func)
        : state(erased_ptr::create<vk::utils::remove_reference_t<F>>(static_cast<F &&>(func)))
        , invoke(+[](erased_ptr const &ptr, Args &&... args) -> R
        {
            return ptr.ref<vk::utils::remove_reference_t<F>>()(static_cast<Args &&>(args)...);
        })
    {}

    R operator()(Args... args) const
    {
        if(nullptr == state)
            throw vk::Exception{VK_ERROR_UNKNOWN, "vk::utils::Function::operator()"};
        return invoke(state, static_cast<Args &&>(args)...);
    }

    operator bool() const noexcept {return nullptr != state;}
};

} // namespace vk::utils
