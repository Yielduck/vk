#pragma once
#include "../vk.h"
#include "type_traits.h"
#include <cassert>
#include <new>
namespace vk::utils
{

template<typename T>
class array
{
    T *pointer;
    std::size_t count;

public:
    ~array()
    {
        if(count == 0u)
            return;

        for(std::size_t i = 0u; i < count; ++i)
            (pointer + i)->~T();
        vk::deallocate(pointer, count * sizeof(T), alignof(T));
    }

    array(std::size_t const size) noexcept
        : pointer(size == 0u
            ? nullptr
            : static_cast<T *>(vk::allocate(size * sizeof(T), alignof(T)))
        )
        , count(size)
    {
        for(std::size_t i = 0u; i < count; ++i)
            new (pointer + i) T();
    }
    array() noexcept
        : array(std::size_t(0))
    {}

    template<typename R>
        requires(requires(R &range) {range.begin(); range.end(); range.size();})
    array(R &&range) noexcept
        : array(std::size_t(range.size()))
    {
        for(std::size_t i = 0; auto &&item : range)
        {
            using U = vk::utils::remove_reference_t<decltype(item)>;
            new (pointer + i++) T(static_cast<U &&>(item));
        }
    }

    array(array<T> const &) = delete;
    array(array<T> &&other) noexcept
        : pointer(other.pointer)
        , count  (other.count  )
    {
        new (&other) array<T>();
    }
    array<T> &operator=(array<T> const &) = delete;
    array<T> &operator=(array<T> &&other) noexcept
    {
        if(this != &other)
        {
            this->~array();
            new (this) array<T>(static_cast<array<T> &&>(other));
        }
        return *this;
    }

    T       &operator[](std::size_t const i)       noexcept {assert(i < count); return pointer[i];}
    T const &operator[](std::size_t const i) const noexcept {assert(i < count); return pointer[i];}

    T       *   data    ()       noexcept {return pointer;}
    T const *   data    () const noexcept {return pointer;}
    std::size_t size    () const noexcept {return count;}
    vk::u32     size32  () const noexcept {assert(count <= UINT32_MAX); return static_cast<vk::u32>(count);}
    bool        empty   () const noexcept {return count == 0u;}

    T       *   begin   ()       noexcept {return pointer;}
    T       *   end     ()       noexcept {return pointer + count;}
    T const *   begin   () const noexcept {return pointer;}
    T const *   end     () const noexcept {return pointer + count;}
};

} // namespace vk::utils
