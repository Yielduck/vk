#pragma once
#include "type_traits.h"
namespace vk::utils
{

template<typename S>
concept GuardState = requires(S const state, typename S::value_t const &ref)
{
    {state.acquire(   )} -> vk::utils::same_as<typename S::value_t>;
    {state.release(ref)} -> vk::utils::same_as<               void>;
};

template<GuardState S>
class Guard : private S
{
public:
    using value_t = typename S::value_t;

    ~Guard() noexcept(false)
    {
        if(alive)
            this->release(value);
    }
    Guard(S state)
        : S(static_cast<S &&>(state))
        , value(this->acquire())
    {}
    Guard(Guard &&other) noexcept
        : S(static_cast<S &&>(other))
        , value(static_cast<value_t &&>(other.value))
    {
        other.alive = false;
    }

    Guard           (Guard const &) = delete;
    Guard &operator=(Guard const &) = delete;
    Guard &operator=(Guard      &&) = delete;

    operator value_t const &() const noexcept {return value;}
    value_t const &     cref() const noexcept {return value;}

private:
    value_t value;
    bool alive = true;
};

} // namespace vk::utils
