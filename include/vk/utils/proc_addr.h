#pragma once
#include <vulkan/vulkan.h>
#define   deviceProcAddr(device  , f) reinterpret_cast<decltype(&f)>(  vkGetDeviceProcAddr(device  , #f))
#define instanceProcAddr(instance, f) reinterpret_cast<decltype(&f)>(vkGetInstanceProcAddr(instance, #f))
