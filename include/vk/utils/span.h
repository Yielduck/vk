#pragma once
#include "../vk.h"
#include <cassert>
namespace vk::utils
{

template<typename T>
class span
{
    T *pointer;
    std::size_t count;

public:
    constexpr span() noexcept
        : pointer(nullptr)
        , count(0u)
    {}

    template<typename R>
        requires(requires(R &range) {range.data(); range.size();})
    constexpr span(R &&range) noexcept
        : pointer(range.data())
        , count(range.size())
    {}
    template<std::size_t N>
    constexpr span(T (&arr)[N]) noexcept
        : pointer(arr)
        , count(N)
    {}
    constexpr span(T * const arr, std::size_t const N) noexcept
        : pointer(arr)
        , count(N)
    {}

    constexpr T &operator[](std::size_t const i) const noexcept {assert(i < count); return pointer[i];}

    constexpr T *           data    () const noexcept {return pointer;}
    constexpr std::size_t   size    () const noexcept {return   count;}

    constexpr bool          empty   () const noexcept {return count == 0u;}

    constexpr T *           begin   () const noexcept {return pointer;}
    constexpr T *           end     () const noexcept {return pointer + count;}

    constexpr T &           first   () const noexcept {return pointer[        0u];}
    constexpr T &           last    () const noexcept {return pointer[count - 1u];}

    constexpr vk::u32       size32  () const noexcept {assert(count <= UINT32_MAX); return static_cast<vk::u32>(count);}
};

} // namespace vk::utils
