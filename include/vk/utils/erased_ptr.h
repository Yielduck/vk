#pragma once
#include "../vk.h"
#include <new>
namespace vk
{

// hello, I'm std::any in disguise!
class erased_ptr
{
    struct AllocInfo
    {
        std::size_t size;
        std::size_t alignment;
    };
    void *pointer = nullptr;
    AllocInfo (*deleter)(void *) = nullptr;

public:
    constexpr erased_ptr() noexcept = default;

    constexpr erased_ptr           (erased_ptr const &) = delete;
    constexpr erased_ptr &operator=(erased_ptr const &) = delete;

    ~erased_ptr()
    {
        if(pointer == nullptr)
            return;
        auto const [size, alignment] = deleter(pointer);
        vk::deallocate(pointer, size, alignment);
    }

    erased_ptr(erased_ptr &&other) noexcept
        : pointer(other.pointer)
        , deleter(other.deleter)
    {
        new (&other) erased_ptr();
    }
    erased_ptr &operator=(erased_ptr &&other) noexcept
    {
        if(this != &other)
        {
            this->~erased_ptr();
            new (this) erased_ptr(static_cast<erased_ptr &&>(other));
        }
        return *this;
    }

    template<typename T, typename... Ts>
    static erased_ptr create(Ts &&... args)
    {
        erased_ptr result;

        result.pointer = vk::allocate(sizeof(T), alignof(T));
        new (result.pointer) T{static_cast<Ts &&>(args)...};

        result.deleter = +[](void * const ptr) noexcept
        {
            static_cast<T *>(ptr)->~T();
            return AllocInfo{sizeof(T), alignof(T)};
        };

        return result;
    }

    constexpr operator void *() const noexcept {return pointer;}

    template<typename T>
    constexpr T *ptr() const noexcept {return  static_cast<T *>(pointer);}
    template<typename T>
    constexpr T &ref() const noexcept {return *static_cast<T *>(pointer);}
};

} // namespace vk
