#pragma once
#include "erased_ptr.h"
#include "tuple.h"
#include <cassert>
namespace vk::utils
{

class StructureChain
{
    erased_ptr pointer = {};
    void const **next = nullptr;

public:
    operator void       *()       noexcept {return pointer;}
    operator void const *() const noexcept {return pointer;}

    StructureChain &append(StructureChain &another) noexcept
        // chain1.append(chain2).append(chain3) returns chain3 ref, so that chain3 -> chain2 -> chain1
    {
        if(another.next == nullptr)
            return *this;

        *(another.next) = pointer;
        return another;
    }

    template<typename... Ts>
    vk::utils::tuple<Ts...>       &asTupleOf()       noexcept {return pointer.ref<vk::utils::tuple<Ts...>>();}
    template<typename... Ts>
    vk::utils::tuple<Ts...> const &asTupleOf() const noexcept {return pointer.ref<vk::utils::tuple<Ts...>>();}

    StructureChain() noexcept = default;

    template<typename... Ts>
    StructureChain(Ts const &... items)
        : pointer(erased_ptr::create<vk::utils::tuple<Ts...>>(items...))
        , next
        (
            [](vk::utils::tuple<Ts...> &tupleRef) noexcept
            {
                auto const chain = []<typename... U>(vk::utils::tuple<U...> &tuple, auto const &self) noexcept
                    -> void const **
                {
                    if constexpr(sizeof...(U) > 1)
                    {
                        tuple.head.pNext = &tuple.tail.head;
                        return self(tuple.tail, self);
                    }
                    tuple.head.pNext = nullptr;
                    return const_cast<void const **>(&tuple.head.pNext);
                };
                return chain(tupleRef, chain);
            }(asTupleOf<Ts...>())
        )
    {
        static_assert(offsetof(vk::utils::tuple<Ts...>, head) == 0);
        assert(pointer == &asTupleOf<Ts...>().head);
    }
};

} // namespace vk::utils
