#pragma once
namespace vk::utils
{

struct  true_type {static constexpr bool value =  true;};
struct false_type {static constexpr bool value = false;};


template<typename T, typename U>
struct is_same       : false_type {};
template<typename T>
struct is_same<T, T> :  true_type {};

template<typename T, typename U>
inline constexpr bool is_same_v = is_same<T, U>::value;

template<typename T, typename U>
concept same_as = is_same_v<T, U>;


template<typename T>
struct is_const          : false_type {};
template<typename T>
struct is_const<T const> :  true_type {};

template<typename T>
inline constexpr bool is_const_v = is_const<T>::value;


template<typename T>
struct remove_reference       {using type = T;};
template<typename T>
struct remove_reference<T  &> {using type = T;};
template<typename T>
struct remove_reference<T &&> {using type = T;};

template<typename T>
using remove_reference_t = remove_reference<T>::type;


template<typename T>
struct remove_cv                   {using type = T;};
template<typename T>
struct remove_cv<T const         > {using type = T;};
template<typename T>
struct remove_cv<T       volatile> {using type = T;};
template<typename T>
struct remove_cv<T const volatile> {using type = T;};

template<typename T>
using remove_cv_t = remove_cv<T>::type;


template<typename T>
using remove_cvref_t = remove_cv_t<remove_reference_t<T>>;

} // namespace vk::utils
