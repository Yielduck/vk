#pragma once
#include "erased_ptr.h"
#include <cassert>
namespace vk::utils
{

template<typename T>
class UniqueHandle
{
    template<typename Destroy>
    struct State
    {
        T handle;
        Destroy destroy;

        ~State() noexcept {destroy(handle);}
    };
    erased_ptr state;

public:
    constexpr UniqueHandle() noexcept = default;

    template<typename Destroy>
    UniqueHandle(T const handle, Destroy destroy)
        : state(erased_ptr::create<State<Destroy>>(handle, static_cast<Destroy &&>(destroy)))
    {
        static_assert(offsetof(State<Destroy>, handle) == 0);
        assert(state.ptr<T>() == &state.ptr<State<Destroy>>()->handle);
    }

    constexpr T       *   operator&   ()          noexcept {return state.ptr<T>();}
    constexpr T const *   operator&   ()  const   noexcept {return state.ptr<T>();}

    constexpr bool        empty       ()  const   noexcept {return state == nullptr;}

    constexpr operator T              ()  const   noexcept {return empty() ? T(0) : state.ref<T>();}
    constexpr T           get         ()  const   noexcept {return empty() ? T(0) : state.ref<T>();}
};

} // namespace vk::utils
