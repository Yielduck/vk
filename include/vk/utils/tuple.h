#pragma once
#include "type_traits.h"
namespace vk::utils
{

template<typename... Ts>
struct tuple
{};
template<typename T, typename... Ts>
struct tuple<T, Ts...>
{
    T head;
    tuple<Ts...> tail;

    tuple(T const &first, Ts const &... rest)
        : head(first  )
        , tail(rest...)
    {}
};

template<typename U, typename T, typename... Ts>
constexpr auto &get(vk::utils::tuple<T, Ts...> &tup) noexcept
{
    if constexpr( vk::utils::is_same_v<U, T>)
        return tup.head;
    if constexpr(!vk::utils::is_same_v<U, T>)
        return get<U>(tup.tail);
}
template<typename U, typename T, typename... Ts>
constexpr auto const &get(vk::utils::tuple<T, Ts...> const &tup) noexcept
{
    if constexpr( vk::utils::is_same_v<U, T>)
        return tup.head;
    if constexpr(!vk::utils::is_same_v<U, T>)
        return get<U>(tup.tail);
}
template<std::size_t I, typename... Ts>
constexpr auto &get(vk::utils::tuple<Ts...> &tup) noexcept
{
    if constexpr(I == 0)
        return tup.head;
    if constexpr(I != 0)
        return get<I - 1>(tup.tail);
}
template<std::size_t I, typename... Ts>
constexpr auto const &get(vk::utils::tuple<Ts...> const &tup) noexcept
{
    if constexpr(I == 0)
        return tup.head;
    if constexpr(I != 0)
        return get<I - 1>(tup.tail);
}

} // namespace vk::utils
