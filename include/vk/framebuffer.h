#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Framebuffer = vk::utils::UniqueHandle<VkFramebuffer>;

struct FramebufferCreateInfo
{
    vk::utils::StructureChain           chain       = {};
    VkFramebufferCreateFlags            flags       = {};
    VkRenderPass                        renderPass;
    vk::utils::span<VkImageView const>  attachment;
    vk::u32                             width;
    vk::u32                             height;
    vk::u32                             layers      = 1u;
};
vk::Framebuffer createFramebuffer(VkDevice, vk::FramebufferCreateInfo const &);

} // namespace vk
