#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Sampler = vk::utils::UniqueHandle<VkSampler>;

struct SamplerCreateInfo
{
    vk::utils::StructureChain   chain                   = {};
    VkSamplerCreateFlags        flags                   = {};
    VkFilter                    magFilter               = VK_FILTER_NEAREST;
    VkFilter                    minFilter               = VK_FILTER_NEAREST;
    VkSamplerMipmapMode         mipmapMode              = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    VkSamplerAddressMode        addressModeU            = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    VkSamplerAddressMode        addressModeV            = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    VkSamplerAddressMode        addressModeW            = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    vk::f32                     mipLodBias              = 0.f;
    VkBool32                    anisotropyEnable        = VK_FALSE;
    vk::f32                     maxAnisotropy           = 0.f;
    VkBool32                    compareEnable           = VK_FALSE;
    VkCompareOp                 compareOp               = VK_COMPARE_OP_NEVER;
    vk::f32                     minLod                  = 0.f;
    vk::f32                     maxLod                  = VK_LOD_CLAMP_NONE;
    VkBorderColor               borderColor             = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    VkBool32                    unnormalizedCoordinates = VK_FALSE;
};
vk::Sampler createSampler(VkDevice, vk::SamplerCreateInfo const & = {});

} // namespace vk
