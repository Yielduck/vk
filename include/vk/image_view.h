#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using ImageView = vk::utils::UniqueHandle<VkImageView>;

struct ImageViewCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkImageViewCreateFlags      flags = {};
    VkImage                     image;
    VkImageViewType             type;
    VkFormat                    format;
    VkComponentMapping          components =
    {
        VK_COMPONENT_SWIZZLE_IDENTITY,
        VK_COMPONENT_SWIZZLE_IDENTITY,
        VK_COMPONENT_SWIZZLE_IDENTITY,
        VK_COMPONENT_SWIZZLE_IDENTITY,
    };
    VkImageSubresourceRange     subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
};
vk::ImageView createImageView(VkDevice, vk::ImageViewCreateInfo const &);

} // namespace vk
