#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Device = vk::utils::UniqueHandle<VkDevice>;

struct DeviceCreateInfo
{
    struct QueueInfo
    {
        vk::utils::StructureChain       chain = {};
        VkDeviceQueueCreateFlags        flags = {};
        vk::u32                         queueFamilyIndex;
        vk::utils::span<vk::f32 const>  priority;
    };

    vk::utils::StructureChain           chain           = {};
    VkDeviceCreateFlags                 flags           = {};
    vk::utils::span<QueueInfo const>    queueInfo;
    vk::utils::span<char const * const> extension       = {};
    VkPhysicalDeviceFeatures const *    enabledFeatures = nullptr;
};
vk::Device createDevice(VkPhysicalDevice, vk::DeviceCreateInfo const &);

VkQueue getDeviceQueue(VkDevice, vk::u32 queueFamily, vk::u32 queueIndex = 0) noexcept;

} // namespace vk
