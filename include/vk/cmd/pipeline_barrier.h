#pragma once
#include "../vk.h"
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk
{

struct MemoryBarrier
{
    vk::utils::StructureChain   chain = {};
    VkAccessFlags               srcAccessMask = 0;
    VkAccessFlags               dstAccessMask = 0;
};
struct BufferMemoryBarrier
{
    vk::utils::StructureChain   chain = {};
    VkAccessFlags               srcAccessMask = 0;
    VkAccessFlags               dstAccessMask = 0;
    vk::u32                     srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    vk::u32                     dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    VkBuffer                    buffer;
    VkDeviceSize                offset  = 0;
    VkDeviceSize                size    = VK_WHOLE_SIZE;
};
struct ImageMemoryBarrier
{
    vk::utils::StructureChain   chain = {};
    VkAccessFlags               srcAccessMask = 0;
    VkAccessFlags               dstAccessMask = 0;
    VkImageLayout               oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout               newLayout;
    vk::u32                     srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    vk::u32                     dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    VkImage                     image;
    VkImageSubresourceRange     subresourceRange =
    {
        .aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel   = 0,
        .levelCount     = 1,
        .baseArrayLayer = 0,
        .layerCount     = 1,
    };
};

struct PipelineBarrierInfo
{
    VkPipelineStageFlags                            srcStageMask    = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    VkPipelineStageFlags                            dstStageMask    = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    VkDependencyFlags                               dependencyFlags = {};
    vk::utils::span<vk::      MemoryBarrier const>  memoryBarrier   = {};
    vk::utils::span<vk::BufferMemoryBarrier const>  bufferBarrier   = {};
    vk::utils::span<vk:: ImageMemoryBarrier const>  imageBarrier    = {};
};

} // namespace vk

namespace vk::cmd
{

void pipelineBarrier(VkCommandBuffer, vk::PipelineBarrierInfo const &);

} // namespace vk::cmd
