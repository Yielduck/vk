#pragma once
#include "../vk.h"
#include "../utils/span.h"
namespace vk::cmd
{

struct CopyBufferInfo
{
    VkBuffer                            srcBuffer;
    VkBuffer                            dstBuffer;
    vk::utils::span<VkBufferCopy const> region;
};
void copyBuffer(VkCommandBuffer, CopyBufferInfo const &) noexcept;

struct CopyImageInfo
{
    VkImage                             srcImage;
    VkImageLayout                       srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    VkImage                             dstImage;
    VkImageLayout                       dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    vk::utils::span<VkImageCopy const>  region;
};
void copyImage(VkCommandBuffer, CopyImageInfo const &) noexcept;

struct CopyBufferToImageInfo
{
    VkBuffer                                    srcBuffer;
    VkImage                                     dstImage;
    VkImageLayout                               dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    vk::utils::span<VkBufferImageCopy const>    region;
};
void copyBufferToImage(VkCommandBuffer, CopyBufferToImageInfo const &) noexcept;

struct CopyImageToBufferInfo
{
    VkImage                                     srcImage;
    VkImageLayout                               srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    VkBuffer                                    dstBuffer;
    vk::utils::span<VkBufferImageCopy const>    region;
};
void copyImageToBuffer(VkCommandBuffer, CopyImageToBufferInfo const &) noexcept;

} // namespace vk::cmd
