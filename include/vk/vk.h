#pragma once
#include <cstdint>
#include <cstddef>
#include <vulkan/vulkan.h>
namespace vk
{

using i32 = std:: int32_t;
using i64 = std:: int64_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using f32 =         float;
using f64 =        double;

static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);

struct PhysicalDeviceMemoryProperties
{
    VkMemoryType type;
    VkMemoryHeap heap;
};

struct Exception
{
    VkResult result;
    char const *functionName;
};

extern VkAllocationCallbacks *pAlloc;

// custom allocation strategy (pools on static memory):
void *    allocate(        std::size_t bytes, std::size_t alignment);
void    deallocate(void *, std::size_t bytes, std::size_t alignment) noexcept;

template<typename T>
struct Allocator
{
    using value_type = T;

    Allocator() = default;

    template<typename U>
    Allocator(Allocator<U> const &) noexcept {}

    [[nodiscard]] T *allocate(std::size_t const count) const
    {
        return static_cast<T *>(vk::allocate(sizeof(T) * count, alignof(T)));
    }
    void deallocate(T * const pointer, std::size_t const count) const noexcept
    {
        return vk::deallocate(pointer, sizeof(T) * count, alignof(T));
    }
};

} // namespace vk
