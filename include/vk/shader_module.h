#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using ShaderModule = vk::utils::UniqueHandle<VkShaderModule>;

struct ShaderModuleCreateInfo
{
    vk::utils::StructureChain       chain = {};
    VkShaderModuleCreateFlags       flags = {};
    vk::utils::span<vk::u32 const>  code;
};

vk::ShaderModule createShaderModule(VkDevice, vk::ShaderModuleCreateInfo const &);

} // namespace vk
