#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
#include "pipeline/shader_stage.h"
namespace vk
{

using ComputePipeline = vk::utils::UniqueHandle<VkPipeline>;

struct ComputePipelineCreateInfo
{
    vk::utils::StructureChain           chain               = {};
    VkPipelineCreateFlags               flags               = {};
    vk::pipeline::ShaderStageCreateInfo stage;
    VkPipelineLayout                    layout;
    VkPipeline                          basePipelineHandle  = VK_NULL_HANDLE;
    vk::i32                             basePipelineIndex   = -1;
};
vk::ComputePipeline createComputePipeline(VkDevice, vk::ComputePipelineCreateInfo const &, VkPipelineCache = VK_NULL_HANDLE);

} // namespace vk
