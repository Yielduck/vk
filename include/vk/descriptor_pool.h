#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using DescriptorPool = vk::utils::UniqueHandle<VkDescriptorPool>;

struct DescriptorPoolCreateInfo
{
    vk::utils::StructureChain                   chain = {};
    VkDescriptorPoolCreateFlags                 flags = {};
    vk::u32                                     maxSets;
    vk::utils::span<VkDescriptorPoolSize const> poolSize;
};
vk::DescriptorPool createDescriptorPool(VkDevice, vk::DescriptorPoolCreateInfo const &);

} // namespace vk
