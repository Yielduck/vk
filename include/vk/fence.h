#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Fence = vk::utils::UniqueHandle<VkFence>;
struct FenceCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkFenceCreateFlags          flags = {};
};
vk::Fence createFence(VkDevice, vk::FenceCreateInfo const & = {});

VkResult waitForFences
(
    VkDevice,
    vk::utils::span<VkFence const>,
    VkBool32 waitAll = VK_TRUE,
    vk::u64 timeout = UINT64_MAX
);

void resetFences(VkDevice, vk::utils::span<VkFence const>);

} // namespace vk
