#pragma once
#include "utils/array.h"
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

vk::utils::array<VkExtensionProperties> enumerateInstanceExtensionProperties(char const *layerName = nullptr);

using Instance = vk::utils::UniqueHandle<VkInstance>;

struct InstanceCreateInfo
{
    vk::utils::StructureChain           chain               = {};
    VkInstanceCreateFlags               flags               = {};
    char const *                        applicationName     = nullptr;
    vk::u32                             applicationVersion  = VK_MAKE_VERSION(1, 0, 0);
    char const *                        engineName          = nullptr;
    vk::u32                             engineVersion       = VK_MAKE_VERSION(1, 0, 0);
    vk::u32                             apiVersion          = VK_API_VERSION_1_0;
    vk::utils::span<char const * const> validationLayer     = {};
    vk::utils::span<char const * const> extension           = {};
};
vk::Instance createInstance(vk::InstanceCreateInfo const & = {});

} // namespace vk
