#pragma once
#include "utils/array.h"
#include "utils/structure_chain.h"
namespace vk
{

vk::utils::array<VkPhysicalDevice> enumeratePhysicalDevices(VkInstance);

struct PhysicalDeviceInfo
{
    VkPhysicalDeviceProperties  properties;
    VkPhysicalDeviceFeatures    features;

    vk::utils::array<vk::PhysicalDeviceMemoryProperties>    memoryProperties;
    vk::utils::array<           VkQueueFamilyProperties>     queueProperties;
    vk::utils::array<             VkExtensionProperties> extensionProperties;
};
vk::PhysicalDeviceInfo physicalDeviceInfo(VkPhysicalDevice);

#ifdef VK_VERSION_1_1
vk::utils::StructureChain getPhysicalDeviceProperties2(VkPhysicalDevice, vk::utils::StructureChain properties) noexcept;
vk::utils::StructureChain getPhysicalDeviceFeatures2  (VkPhysicalDevice, vk::utils::StructureChain features  ) noexcept;
#endif

} // namespace vk
