#pragma once
#include "../utils/guard.h"
#include "../utils/structure_chain.h"
namespace vk
{

struct CommandBufferInheritanceInfo
{
    vk::utils::StructureChain       chain                   = {};
    VkRenderPass                    renderPass              = VK_NULL_HANDLE;
    vk::u32                         subpass                 = 0u;
    VkFramebuffer                   framebuffer             = VK_NULL_HANDLE;
    VkBool32                        occlusionQueryEnable    = VK_FALSE;
    VkQueryControlFlags             queryFlags              = {};
    VkQueryPipelineStatisticFlags   pipelineStatistics      = {};
};

} // namespace vk

namespace vk::guard
{

struct CommandBufferState
{
    vk::utils::StructureChain           chain           = {};
    VkCommandBufferUsageFlags           flags           = {};
    vk::CommandBufferInheritanceInfo    inheritanceInfo = {};

    VkCommandBuffer                     commandBuffer;

    using value_t = VkCommandBuffer;
    value_t acquire(               ) const;
    void    release(value_t const &) const;
};

using CommandBuffer = vk::utils::Guard<vk::guard::CommandBufferState>;

} // namespace vk::guard
