#pragma once
#include "../vk.h"
#include "../utils/guard.h"
namespace vk::guard
{

namespace detail
{

template<typename S>
auto acquire(S const &state)
{
    void *ptr = nullptr;

    {
        VkResult const result = vkMapMemory(state.device, state.memory, state.offset, state.size, {}, &ptr);
        if(VK_SUCCESS != result)
            throw vk::Exception{result, "vkMapMemory"};
    }

    {
        VkMappedMemoryRange const range =
        {
            .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
            .pNext = nullptr,
            .memory = state.memory,
            .offset = state.offset,
            .size = state.size,
        };
        VkResult const result = vkInvalidateMappedMemoryRanges(state.device, 1u, &range);
        if(VK_SUCCESS != result)
        {
            vkUnmapMemory(state.device, state.memory);
            throw vk::Exception{result, "vkInvalidateMappedMemoryRanges"};
        }
    }

    return static_cast<typename S::value_t>(ptr);
}
template<typename S>
void release(S const &state)
{
    if constexpr(S::isConst == false)
    {
        VkMappedMemoryRange const range =
        {
            .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
            .pNext = nullptr,
            .memory = state.memory,
            .offset = state.offset,
            .size = state.size,
        };
        VkResult const result = vkFlushMappedMemoryRanges(state.device, 1, &range);
        vkUnmapMemory(state.device, state.memory);
        if(VK_SUCCESS != result)
            throw vk::Exception{result, "vkFlushMappedMemoryRanges"};
    }
    else
        vkUnmapMemory(state.device, state.memory);
}

} // namespace detail

template<typename T>
struct MemoryMapState
{
    VkDevice        device;
    VkDeviceMemory  memory;
    VkDeviceSize    offset = 0u;
    VkDeviceSize    size = VK_WHOLE_SIZE;

    using value_t = T *;
    value_t acquire(               ) const {return detail::acquire(*this);}
    void    release(value_t const &) const {return detail::release(*this);}

    static constexpr bool isConst = vk::utils::is_const_v<T>;
};

// sensitive to const-ness of T (flushes if not const)
template<typename T>
using MemoryMap = vk::utils::Guard<vk::guard::MemoryMapState<T>>;

} // namespace vk::guard
