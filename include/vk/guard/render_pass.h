#pragma once
#include "../utils/guard.h"
#include "../utils/span.h"
#include "../utils/structure_chain.h"
namespace vk::guard
{

struct RenderPassState
{
    vk::utils::StructureChain           chain           = {};
    VkCommandBuffer                     commandBuffer;
    VkSubpassContents                   contents        = VK_SUBPASS_CONTENTS_INLINE;

    VkRenderPass                        renderPass;
    VkFramebuffer                       framebuffer;
    VkRect2D                            renderArea;
    vk::utils::span<VkClearValue const> clearValue      = {};

    struct value_t
    {
        VkCommandBuffer buffer;
        VkRenderPass    pass;
    };
    value_t acquire(               ) const;
    void    release(value_t const &) const;
};

using RenderPass = vk::utils::Guard<vk::guard::RenderPassState>;

} // namespace vk::guard
