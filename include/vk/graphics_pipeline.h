#pragma once
#include "pipeline/color_blend_state.h"
#include "pipeline/depth_stencil_state.h"
#include "pipeline/dynamic_state.h"
#include "pipeline/input_assembly_state.h"
#include "pipeline/multisample_state.h"
#include "pipeline/rasterization_state.h"
#include "pipeline/shader_stage.h"
#include "pipeline/tessellation_state.h"
#include "pipeline/vertex_input_state.h"
#include "pipeline/viewport_state.h"
#include "utils/unique_handle.h"
namespace vk
{

using GraphicsPipeline = vk::utils::UniqueHandle<VkPipeline>;

struct GraphicsPipelineCreateInfo
{
    vk::utils::StructureChain                                   chain               = {};
    VkPipelineCreateFlags                                       flags               = {};
    vk::utils::span<vk::pipeline::ShaderStageCreateInfo const>  stage;
    vk::pipeline::  VertexInputStateCreateInfo                  vertexInputState    = {};
    vk::pipeline::InputAssemblyStateCreateInfo                  inputAssemblyState;
    vk::pipeline:: TessellationStateCreateInfo                  tessellationState   = {};
    vk::pipeline::     ViewportStateCreateInfo                  viewportState       = {};
    vk::pipeline::RasterizationStateCreateInfo                  rasterizationState  = {};
    vk::pipeline::  MultisampleStateCreateInfo                  multisampleState    = {};
    vk::pipeline:: DepthStencilStateCreateInfo                  depthStencilState   = {};
    vk::pipeline::   ColorBlendStateCreateInfo                  colorBlendState     = {};
    vk::pipeline::      DynamicStateCreateInfo                  dynamicState        = {};
    VkPipelineLayout                                            layout;
    VkRenderPass                                                renderPass;
    vk::u32                                                     subpass             = 0;
    VkPipeline                                                  basePipelineHandle  = VK_NULL_HANDLE;
    vk::i32                                                     basePipelineIndex   = -1;
};
vk::GraphicsPipeline createGraphicsPipeline(VkDevice, vk::GraphicsPipelineCreateInfo const &, VkPipelineCache = VK_NULL_HANDLE);

} // namespace vk
