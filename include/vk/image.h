#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Image = vk::utils::UniqueHandle<VkImage>;

struct ImageCreateInfo
{
    vk::utils::StructureChain       chain           = {};
    VkImageCreateFlags              flags           = {};
    VkImageType                     type;
    VkFormat                        format;
    VkExtent3D                      extent;
    vk::u32                         mipLevels       = 1;
    vk::u32                         arrayLayers     = 1;
    VkSampleCountFlagBits           samples         = VK_SAMPLE_COUNT_1_BIT;
    VkImageTiling                   tiling          = VK_IMAGE_TILING_OPTIMAL;
    VkImageUsageFlags               usage;
    vk::utils::span<vk::u32 const>  queueFamily     = {};
    VkImageLayout                   initialLayout   = VK_IMAGE_LAYOUT_UNDEFINED;
};
vk::Image createImage(VkDevice, vk::ImageCreateInfo const &);

VkMemoryRequirements imageMemoryRequirements(VkDevice, VkImage) noexcept;
void bindImageMemory(VkDevice, VkImage, VkDeviceMemory, VkDeviceSize offset = 0);

} // namespace vk
