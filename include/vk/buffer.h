#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using Buffer = vk::utils::UniqueHandle<VkBuffer>;

struct BufferCreateInfo
{
    vk::utils::StructureChain       chain = {};
    VkBufferCreateFlags             flags = {};
    VkDeviceSize                    size;
    VkBufferUsageFlags              usage;
    vk::utils::span<vk::u32 const>  queueFamily = {};
};
vk::Buffer createBuffer(VkDevice, vk::BufferCreateInfo const &);

VkMemoryRequirements bufferMemoryRequirements(VkDevice, VkBuffer) noexcept;
void bindBufferMemory(VkDevice, VkBuffer, VkDeviceMemory, VkDeviceSize offset = 0);

} // namespace vk
