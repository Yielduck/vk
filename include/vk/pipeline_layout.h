#pragma once
#include "utils/span.h"
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using PipelineLayout = vk::utils::UniqueHandle<VkPipelineLayout>;

struct PipelineLayoutCreateInfo
{
    vk::utils::StructureChain                       chain               = {};
    VkPipelineLayoutCreateFlags                     flags               = {};
    vk::utils::span<VkDescriptorSetLayout const>    setLayout           = {};
    vk::utils::span<VkPushConstantRange   const>    pushConstantRange   = {};
};
vk::PipelineLayout createPipelineLayout(VkDevice, vk::PipelineLayoutCreateInfo const & = {});

} // namespace vk
