#pragma once
#include "utils/structure_chain.h"
#include "utils/unique_handle.h"
namespace vk
{

using DeviceMemory = vk::utils::UniqueHandle<VkDeviceMemory>;

struct DeviceMemoryCreateInfo
{
    vk::utils::StructureChain   chain = {};
    VkDeviceSize                allocationSize;
    vk::u32                     memoryTypeIndex;
};
vk::DeviceMemory createDeviceMemory(VkDevice, vk::DeviceMemoryCreateInfo const &);

} // namespace vk
