cmake_minimum_required(VERSION 3.21) # required by Vulkan::Headers
project(vk LANGUAGES CXX VERSION 0.0.6)

add_library(${PROJECT_NAME})
target_sources(${PROJECT_NAME} PUBLIC
    ./include/vk/buffer.h
    ./include/vk/buffer_view.h
    ./include/vk/cmd/copy.h
    ./include/vk/cmd/pipeline_barrier.h
    ./include/vk/command_buffer.h
    ./include/vk/command_pool.h
    ./include/vk/compute_pipeline.h
    ./include/vk/descriptor_pool.h
    ./include/vk/descriptor_set.h
    ./include/vk/descriptor_set_layout.h
    ./include/vk/device.h
    ./include/vk/device_memory.h
    ./include/vk/ext/find.h
    ./include/vk/ext/glfw_surface.h
    ./include/vk/ext/guard/glfw.h
    ./include/vk/ext/guard/tracy_command_buffer.h
    ./include/vk/ext/guard/tracy_frame.h
    ./include/vk/ext/imgui/context.h
    ./include/vk/ext/imgui/frame.h
    ./include/vk/ext/imgui/render_surface.h
    ./include/vk/ext/swapchain.h
    ./include/vk/ext/tracy.h
    ./include/vk/ext/tracy_context.h
    ./include/vk/ext/trivial/framebuffer_chain.h
    ./include/vk/ext/trivial/device.h
    ./include/vk/ext/trivial/instance.h
    ./include/vk/fence.h
    ./include/vk/framebuffer.h
    ./include/vk/graphics_pipeline.h
    ./include/vk/guard/command_buffer.h
    ./include/vk/guard/memory_map.h
    ./include/vk/guard/render_pass.h
    ./include/vk/image.h
    ./include/vk/image_view.h
    ./include/vk/instance.h
    ./include/vk/khr/buffer_address.h
    ./include/vk/khr/pipeline_executable_info.h
    ./include/vk/khr/queue_present.h
    ./include/vk/khr/swapchain.h
    ./include/vk/physical_device_info.h
    ./include/vk/pipeline/color_blend_state.h
    ./include/vk/pipeline/depth_stencil_state.h
    ./include/vk/pipeline/dynamic_state.h
    ./include/vk/pipeline/input_assembly_state.h
    ./include/vk/pipeline/multisample_state.h
    ./include/vk/pipeline/rasterization_state.h
    ./include/vk/pipeline/shader_stage.h
    ./include/vk/pipeline/tessellation_state.h
    ./include/vk/pipeline/vertex_input_state.h
    ./include/vk/pipeline/viewport_state.h
    ./include/vk/pipeline_layout.h
    ./include/vk/queue_submit.h
    ./include/vk/render_pass.h
    ./include/vk/sampler.h
    ./include/vk/semaphore.h
    ./include/vk/shader_module.h
    ./include/vk/utils/array.h
    ./include/vk/utils/erased_ptr.h
    ./include/vk/utils/function.h
    ./include/vk/utils/guard.h
    ./include/vk/utils/proc_addr.h
    ./include/vk/utils/span.h
    ./include/vk/utils/structure_chain.h
    ./include/vk/utils/tuple.h
    ./include/vk/utils/type_traits.h
    ./include/vk/utils/unique_handle.h
    ./include/vk/vk.h
)

target_sources(${PROJECT_NAME} PRIVATE
    ./src/buffer.cpp
    ./src/buffer_view.cpp
    ./src/cmd/copy.cpp
    ./src/cmd/pipeline_barrier.cpp
    ./src/command_buffer.cpp
    ./src/command_pool.cpp
    ./src/compute_pipeline.cpp
    ./src/descriptor_pool.cpp
    ./src/descriptor_set.cpp
    ./src/descriptor_set_layout.cpp
    ./src/device.cpp
    ./src/device_memory.cpp
    ./src/fence.cpp
    ./src/framebuffer.cpp
    ./src/graphics_pipeline.cpp
    ./src/guard/command_buffer.cpp
    ./src/guard/render_pass.cpp
    ./src/image.cpp
    ./src/image_view.cpp
    ./src/instance.cpp
    ./src/khr/buffer_address.cpp
    ./src/khr/pipeline_executable_info.cpp
    ./src/khr/queue_present.cpp
    ./src/khr/swapchain.cpp
    ./src/physical_device_info.cpp
    ./src/pipeline_layout.cpp
    ./src/queue_submit.cpp
    ./src/render_pass.cpp
    ./src/sampler.cpp
    ./src/semaphore.cpp
    ./src/shader_module.cpp
    ./src/vk.cpp
)

if(NOT DEFINED VK_STATIC_MEMORY_SIZE)
    set(VK_STATIC_MEMORY_SIZE 262144)
endif()
if(NOT DEFINED VK_NO_HEAP)
    set(VK_NO_HEAP false)
endif()
message(STATUS "VK_STATIC_MEMORY_SIZE=" ${VK_STATIC_MEMORY_SIZE})
message(STATUS "VK_NO_HEAP=" ${VK_NO_HEAP})
target_compile_definitions(${PROJECT_NAME} PRIVATE VK_STATIC_MEMORY_SIZE=${VK_STATIC_MEMORY_SIZE} VK_NO_HEAP=${VK_NO_HEAP})

find_package(Vulkan REQUIRED)
target_link_libraries(${PROJECT_NAME} PUBLIC Vulkan::Headers)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_20)
target_include_directories(${PROJECT_NAME} PUBLIC ./include)

if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
   CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -pedantic -Wshadow -Wconversion -Wsign-conversion)
endif()
