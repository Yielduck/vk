#include <vk/command_buffer.h>
namespace vk
{

vk::CommandBuffer createCommandBuffer(VkDevice const device, vk::CommandBufferCreateInfo const &info)
{
    VkCommandBufferAllocateInfo const commandBufferAllocateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext              = info.chain,
        .commandPool        = info.commandPool,
        .level              = info.level,
        .commandBufferCount = 1,
    };
    VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
    VkResult const result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkAllocateCommandBuffers"};
    return
    {
        commandBuffer,
        [device, pool = info.commandPool](VkCommandBuffer const handle) noexcept
        {
            vkFreeCommandBuffers(device, pool, 1, &handle);
        },
    };
}

} // namespace vk
