#include <vk/guard/command_buffer.h>
namespace vk::guard
{

VkCommandBuffer CommandBufferState::acquire() const
{
    VkCommandBufferInheritanceInfo const commandBufferInheritanceInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        .pNext                  = inheritanceInfo.chain,
        .renderPass             = inheritanceInfo.renderPass,
        .subpass                = inheritanceInfo.subpass,
        .framebuffer            = inheritanceInfo.framebuffer,
        .occlusionQueryEnable   = inheritanceInfo.occlusionQueryEnable,
        .queryFlags             = inheritanceInfo.queryFlags,
        .pipelineStatistics     = inheritanceInfo.pipelineStatistics,
    };
    VkCommandBufferBeginInfo const commandBufferBeginInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext              = chain,
        .flags              = flags,
        .pInheritanceInfo   = &commandBufferInheritanceInfo,
    };
    VkResult const result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkBeginCommandBuffer"};
    return commandBuffer;
}
void CommandBufferState::release(value_t const &) const
{
    VkResult const result = vkEndCommandBuffer(commandBuffer);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkEndCommandBuffer"};
}

} // namespace vk::guard
