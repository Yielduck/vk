#include <vk/guard/render_pass.h>
namespace vk::guard
{

RenderPassState::value_t RenderPassState::acquire() const
{
    VkRenderPassBeginInfo const renderPassBeginInfo =
    {
        .sType            = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext            = chain,
        .renderPass       = renderPass,
        .framebuffer      = framebuffer,
        .renderArea       = renderArea,
        .clearValueCount  = clearValue.size32(),
        .pClearValues     = clearValue.data(),
    };
    vkCmdBeginRenderPass(commandBuffer, &renderPassBeginInfo, contents);
    return
    {
        .buffer = commandBuffer,
        .pass   = renderPass,
    };
}
void RenderPassState::release(value_t const &) const
{
    vkCmdEndRenderPass(commandBuffer);
}

} // namespace vk::guard
