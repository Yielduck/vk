#include <vk/descriptor_set_layout.h>
namespace vk
{

vk::DescriptorSetLayout createDescriptorSetLayout(VkDevice const device, vk::DescriptorSetLayoutCreateInfo const &info)
{
    VkDescriptorSetLayoutCreateInfo const descriptorSetLayoutCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext          = info.chain,
        .flags          = info.flags,
        .bindingCount   = info.binding.size32(),
        .pBindings      = info.binding.data(),
    };
    VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
    VkResult const result = vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, pAlloc, &descriptorSetLayout);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateDescriptorSetLayout"};
    return
    {
        descriptorSetLayout,
        [device](VkDescriptorSetLayout const handle) noexcept {vkDestroyDescriptorSetLayout(device, handle, pAlloc);},
    };
}

} // namespace vk
