#include <vk/device_memory.h>
namespace vk
{

vk::DeviceMemory createDeviceMemory(VkDevice const device, vk::DeviceMemoryCreateInfo const &info)
{
    VkMemoryAllocateInfo const allocateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext              = info.chain,
        .allocationSize     = info.allocationSize,
        .memoryTypeIndex    = info.memoryTypeIndex,
    };
    VkDeviceMemory deviceMemory = VK_NULL_HANDLE;
    VkResult const result = vkAllocateMemory(device, &allocateInfo, pAlloc, &deviceMemory);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkAllocateMemory"};
    return
    {
        deviceMemory,
        [device](VkDeviceMemory const handle) noexcept {vkFreeMemory(device, handle, pAlloc);},
    };
}

} // namespace vk
