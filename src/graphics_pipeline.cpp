#include <vk/graphics_pipeline.h>
#include <vk/utils/array.h>
namespace vk
{

vk::GraphicsPipeline createGraphicsPipeline
(
    VkDevice const device,
    vk::GraphicsPipelineCreateInfo const &info,
    VkPipelineCache const cache
)
{
    vk::u32 const stageCount = info.stage.size32();
    vk::utils::array<VkSpecializationInfo           > specInfo   (stageCount);
    vk::utils::array<VkPipelineShaderStageCreateInfo> shaderStage(stageCount);
    for(vk::u32 i = 0u; i < stageCount; ++i)
    {
        auto const &mapEntry = info.stage[i].mapEntry;
        specInfo[i] =
        {
            .mapEntryCount  = mapEntry.size32(),
            .pMapEntries    = mapEntry.data(),
            .dataSize       = mapEntry.size32() != 0u
                ? mapEntry.last().offset + mapEntry.last().size
                : 0u,
            .pData          = info.stage[i].data,
        };
        shaderStage[i] =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext                  = info.stage[i].chain,
            .flags                  = info.stage[i].flags,
            .stage                  = info.stage[i].stage,
            .module                 = info.stage[i].module,
            .pName                  = info.stage[i].name,
            .pSpecializationInfo    = mapEntry.size32() != 0u
                ? specInfo.data() + i
                : nullptr,
        };
    }

    VkPipelineVertexInputStateCreateInfo const vertexInputState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext                              = info.vertexInputState.chain,
        .flags                              = info.vertexInputState.flags,
        .vertexBindingDescriptionCount      = info.vertexInputState.binding.size32(),
        .pVertexBindingDescriptions         = info.vertexInputState.binding.data(),
        .vertexAttributeDescriptionCount    = info.vertexInputState.attribute.size32(),
        .pVertexAttributeDescriptions       = info.vertexInputState.attribute.data(),
    };
    VkPipelineInputAssemblyStateCreateInfo const inputAssemblyState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext                  = info.inputAssemblyState.chain,
        .flags                  = info.inputAssemblyState.flags,
        .topology               = info.inputAssemblyState.topology,
        .primitiveRestartEnable = info.inputAssemblyState.primitiveRestartEnable,
    };
    VkPipelineTessellationStateCreateInfo const tessellationState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO,
        .pNext              = info.tessellationState.chain,
        .flags              = info.tessellationState.flags,
        .patchControlPoints = info.tessellationState.patchControlPoints,
    };
    VkPipelineViewportStateCreateInfo const viewportState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext          = info.viewportState.chain,
        .flags          = info.viewportState.flags,
        .viewportCount  = info.viewportState.viewport.size32(),
        .pViewports     = info.viewportState.viewport.data(),
        .scissorCount   = info.viewportState.scissor.size32(),
        .pScissors      = info.viewportState.scissor.data(),
    };
    VkPipelineRasterizationStateCreateInfo const rasterizationState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext                      = info.rasterizationState.chain,
        .flags                      = info.rasterizationState.flags,
        .depthClampEnable           = info.rasterizationState.depthClampEnable,
        .rasterizerDiscardEnable    = info.rasterizationState.rasterizerDiscardEnable,
        .polygonMode                = info.rasterizationState.polygonMode,
        .cullMode                   = info.rasterizationState.cullMode,
        .frontFace                  = info.rasterizationState.frontFace,
        .depthBiasEnable            = info.rasterizationState.depthBiasEnable,
        .depthBiasConstantFactor    = info.rasterizationState.depthBiasConstantFactor,
        .depthBiasClamp             = info.rasterizationState.depthBiasClamp,
        .depthBiasSlopeFactor       = info.rasterizationState.depthBiasSlopeFactor,
        .lineWidth                  = info.rasterizationState.lineWidth,
    };
    VkPipelineMultisampleStateCreateInfo const multisampleState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext                  = info.multisampleState.chain,
        .flags                  = info.multisampleState.flags,
        .rasterizationSamples   = info.multisampleState.rasterizationSamples,
        .sampleShadingEnable    = info.multisampleState.sampleShadingEnable,
        .minSampleShading       = info.multisampleState.minSampleShading,
        .pSampleMask            = info.multisampleState.sampleMask.data(),
        .alphaToCoverageEnable  = info.multisampleState.alphaToCoverageEnable,
        .alphaToOneEnable       = info.multisampleState.alphaToOneEnable,
    };
    VkPipelineDepthStencilStateCreateInfo const depthStencilState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext                  = info.depthStencilState.chain,
        .flags                  = info.depthStencilState.flags,
        .depthTestEnable        = info.depthStencilState.depthTest,
        .depthWriteEnable       = info.depthStencilState.depthWrite,
        .depthCompareOp         = info.depthStencilState.depthCompare,
        .depthBoundsTestEnable  = info.depthStencilState.depthBoundsTest,
        .stencilTestEnable      = info.depthStencilState.stencilTest,
        .front                  = info.depthStencilState.front,
        .back                   = info.depthStencilState.back,
        .minDepthBounds         = info.depthStencilState.minDepthBounds,
        .maxDepthBounds         = info.depthStencilState.maxDepthBounds,
    };
    static_assert(sizeof(vk::pipeline::ColorBlendAttachmentState) == sizeof(VkPipelineColorBlendAttachmentState));
    VkPipelineColorBlendStateCreateInfo const colorBlendState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext              = info.colorBlendState.chain,
        .flags              = info.colorBlendState.flags,
        .logicOpEnable      = info.colorBlendState.logicOpEnable,
        .logicOp            = info.colorBlendState.logicOp,
        .attachmentCount    = info.colorBlendState.attachment.size32(),
        .pAttachments       = reinterpret_cast<VkPipelineColorBlendAttachmentState const *>(info.colorBlendState.attachment.data()), // god please forgive me
        .blendConstants     =
        {
            info.colorBlendState.blendConstants[0],
            info.colorBlendState.blendConstants[1],
            info.colorBlendState.blendConstants[2],
            info.colorBlendState.blendConstants[3],
        },
    };
    VkPipelineDynamicStateCreateInfo const dynamicState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext              = info.dynamicState.chain,
        .flags              = info.dynamicState.flags,
        .dynamicStateCount  = info.dynamicState.dynamicState.size32(),
        .pDynamicStates     = info.dynamicState.dynamicState.data(),
    };

    VkGraphicsPipelineCreateInfo const pipelineInfo =
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext                  = info.chain,
        .flags                  = info.flags,
        .stageCount             = shaderStage.size32(),
        .pStages                = shaderStage.data(),
        .pVertexInputState      = &  vertexInputState,
        .pInputAssemblyState    = &inputAssemblyState,
        .pTessellationState     = & tessellationState,
        .pViewportState         = &     viewportState,
        .pRasterizationState    = &rasterizationState,
        .pMultisampleState      = &  multisampleState,
        .pDepthStencilState     = & depthStencilState,
        .pColorBlendState       = &   colorBlendState,
        .pDynamicState          = &      dynamicState,
        .layout                 = info.layout,
        .renderPass             = info.renderPass,
        .subpass                = info.subpass,
        .basePipelineHandle     = info.basePipelineHandle,
        .basePipelineIndex      = info.basePipelineIndex,
    };
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkResult const result = vkCreateGraphicsPipelines(device, cache, 1, &pipelineInfo, pAlloc, &pipeline);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateGraphicsPipelines"};
    return
    {
        pipeline,
        [device](VkPipeline const handle) noexcept {vkDestroyPipeline(device, handle, pAlloc);}
    };
}

} // namespace vk
