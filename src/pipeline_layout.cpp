#include <vk/pipeline_layout.h>
namespace vk
{

vk::PipelineLayout createPipelineLayout(VkDevice const device, vk::PipelineLayoutCreateInfo const &info)
{
    VkPipelineLayoutCreateInfo const pipelineLayoutCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = {},
        .setLayoutCount         = info.setLayout.size32(),
        .pSetLayouts            = info.setLayout.data(),
        .pushConstantRangeCount = info.pushConstantRange.size32(),
        .pPushConstantRanges    = info.pushConstantRange.data(),
    };
    VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
    VkResult const result = vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, pAlloc, &pipelineLayout);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreatePipelineLayout"};
    return
    {
        pipelineLayout,
        [device](VkPipelineLayout const handle) noexcept {vkDestroyPipelineLayout(device, handle, pAlloc);},
    };
}

} // namespace vk
