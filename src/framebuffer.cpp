#include <vk/framebuffer.h>
namespace vk
{

vk::Framebuffer createFramebuffer(VkDevice const device, vk::FramebufferCreateInfo const &info)
{
    VkFramebufferCreateInfo const framebufferCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext              = info.chain,
        .flags              = info.flags,
        .renderPass         = info.renderPass,
        .attachmentCount    = info.attachment.size32(),
        .pAttachments       = info.attachment.data(),
        .width              = info.width,
        .height             = info.height,
        .layers             = info.layers,
    };
    VkFramebuffer framebuffer = VK_NULL_HANDLE;
    VkResult const result = vkCreateFramebuffer(device, &framebufferCreateInfo, pAlloc, &framebuffer);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateFramebuffer"};
    return
    {
        framebuffer,
        [device](VkFramebuffer const handle) noexcept {vkDestroyFramebuffer(device, handle, pAlloc);}
    };
}

} // namespace vk
