#include <vk/shader_module.h>
namespace vk
{

vk::ShaderModule createShaderModule(VkDevice const device, vk::ShaderModuleCreateInfo const &info)
{
    VkShaderModuleCreateInfo const shaderModuleCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext      = info.chain,
        .flags      = info.flags,
        .codeSize   = info.code.size32() * 4u,
        .pCode      = info.code.data(),
    };
    VkShaderModule moduleHandle = VK_NULL_HANDLE;
    VkResult const result = vkCreateShaderModule(device, &shaderModuleCreateInfo, pAlloc, &moduleHandle);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateShaderModule"};
    return
    {
        moduleHandle,
        [device](VkShaderModule const handle) noexcept {vkDestroyShaderModule(device, handle, pAlloc);},
    };
}

} // namespace vk
