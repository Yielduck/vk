#include <vk/sampler.h>
namespace vk
{

vk::Sampler createSampler(VkDevice const device, vk::SamplerCreateInfo const &info)
{
    VkSamplerCreateInfo const samplerCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext                      = info.chain,
        .flags                      = info.flags,
        .magFilter                  = info.magFilter,
        .minFilter                  = info.minFilter,
        .mipmapMode                 = info.mipmapMode,
        .addressModeU               = info.addressModeU,
        .addressModeV               = info.addressModeV,
        .addressModeW               = info.addressModeW,
        .mipLodBias                 = info.mipLodBias,
        .anisotropyEnable           = info.anisotropyEnable,
        .maxAnisotropy              = info.maxAnisotropy,
        .compareEnable              = info.compareEnable,
        .compareOp                  = info.compareOp,
        .minLod                     = info.minLod,
        .maxLod                     = info.maxLod,
        .borderColor                = info.borderColor,
        .unnormalizedCoordinates    = info.unnormalizedCoordinates,
    };
    VkSampler sampler = VK_NULL_HANDLE;
    VkResult const result = vkCreateSampler(device, &samplerCreateInfo, pAlloc, &sampler);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateSampler"};
    return
    {
        sampler,
        [device](VkSampler const handle) noexcept {vkDestroySampler(device, handle, pAlloc);}
    };
}

} // namespace vk
