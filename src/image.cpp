#include <vk/image.h>
namespace vk
{

vk::Image createImage(VkDevice const device, vk::ImageCreateInfo const &info)
{
    VkImageCreateInfo const imageCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext                  = info.chain,
        .flags                  = info.flags,
        .imageType              = info.type,
        .format                 = info.format,
        .extent                 = info.extent,
        .mipLevels              = info.mipLevels,
        .arrayLayers            = info.arrayLayers,
        .samples                = info.samples,
        .tiling                 = info.tiling,
        .usage                  = info.usage,
        .sharingMode            = info.queueFamily.size32() == 0u
            ? VK_SHARING_MODE_EXCLUSIVE
            : VK_SHARING_MODE_CONCURRENT,
        .queueFamilyIndexCount  = info.queueFamily.size32(),
        .pQueueFamilyIndices    = info.queueFamily.data(),
        .initialLayout          = info.initialLayout,
    };
    VkImage image = VK_NULL_HANDLE;
    VkResult const result = vkCreateImage(device, &imageCreateInfo, pAlloc, &image);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateImage"};
    return
    {
        image,
        [device](VkImage const handle) noexcept {vkDestroyImage(device, handle, pAlloc);},
    };
}
VkMemoryRequirements imageMemoryRequirements(VkDevice const device, VkImage const image) noexcept
{
    VkMemoryRequirements requirements;
    vkGetImageMemoryRequirements(device, image, &requirements);
    return requirements;
}
void bindImageMemory(VkDevice const device, VkImage const image, VkDeviceMemory const memory, VkDeviceSize const offset)
{
    VkResult const result = vkBindImageMemory(device, image, memory, offset);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkBindImageMemory"};
}

} // namespace vk
