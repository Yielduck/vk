#include <vk/compute_pipeline.h>
namespace vk
{

vk::ComputePipeline createComputePipeline
(
    VkDevice const device,
    vk::ComputePipelineCreateInfo const &info,
    VkPipelineCache const cache
)
{
    auto const &mapEntry = info.stage.mapEntry;
    VkSpecializationInfo const specInfo =
    {
        .mapEntryCount  = mapEntry.size32(),
        .pMapEntries    = mapEntry.data  (),
        .dataSize       = mapEntry.size32() != 0u
            ? mapEntry.last().offset + mapEntry.last().size
            : 0u,
        .pData          = info.stage.data,
    };
    VkPipelineShaderStageCreateInfo const stage =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext                  = info.stage.chain,
        .flags                  = info.stage.flags,
        .stage                  = info.stage.stage,
        .module                 = info.stage.module,
        .pName                  = info.stage.name,
        .pSpecializationInfo    = mapEntry.size32() != 0u
            ? &specInfo
            : nullptr,
    };
    VkComputePipelineCreateInfo const pipelineCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .pNext              = info.chain,
        .flags              = info.flags,
        .stage              = stage,
        .layout             = info.layout,
        .basePipelineHandle = info.basePipelineHandle,
        .basePipelineIndex  = info.basePipelineIndex,
    };
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkResult const result = vkCreateComputePipelines(device, cache, 1, &pipelineCreateInfo, pAlloc, &pipeline);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateComputePipelines"};
    return
    {
        pipeline,
        [device](VkPipeline const handle) noexcept {vkDestroyPipeline(device, handle, pAlloc);},
    };
}

} // namespace vk
