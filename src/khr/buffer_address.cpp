#include <vk/khr/buffer_address.h>
#include <vk/utils/proc_addr.h>
namespace vk::khr
{

#ifdef VK_KHR_buffer_device_address
VkDeviceAddress getBufferDeviceAddress(VkDevice const device, VkBuffer const buffer) noexcept
{
    VkBufferDeviceAddressInfoKHR const info =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_KHR,
        .pNext = nullptr,
        .buffer = buffer,
    };
    return deviceProcAddr(device, vkGetBufferDeviceAddressKHR)(device, &info);
}
vk::u64 getBufferOpaqueCaptureAddress(VkDevice const device, VkBuffer const buffer) noexcept
{
    VkBufferDeviceAddressInfoKHR const info =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_KHR,
        .pNext = nullptr,
        .buffer = buffer,
    };
    return deviceProcAddr(device, vkGetBufferOpaqueCaptureAddressKHR)(device, &info);
}
#endif

} // namespace vk::khr

namespace vk
{

#ifdef VK_API_VERSION_1_2
VkDeviceAddress getBufferDeviceAddress(VkDevice const device, VkBuffer const buffer) noexcept
{
    VkBufferDeviceAddressInfo const info =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext = nullptr,
        .buffer = buffer,
    };
    return vkGetBufferDeviceAddress(device, &info);
}
vk::u64 getBufferOpaqueCaptureAddress(VkDevice const device, VkBuffer const buffer) noexcept
{
    VkBufferDeviceAddressInfo const info =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext = nullptr,
        .buffer = buffer,
    };
    return vkGetBufferOpaqueCaptureAddress(device, &info);
}
#endif

} // namespace vk
