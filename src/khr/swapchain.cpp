#include <vk/khr/swapchain.h>
namespace vk::khr
{

#ifdef VK_KHR_swapchain
vk::khr::Swapchain createSwapchain(VkDevice const device, vk::khr::SwapchainCreateInfo const &info)
{
    VkSwapchainCreateInfoKHR const swapchainCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext                  = info.chain,
        .flags                  = info.flags,
        .surface                = info.surface,
        .minImageCount          = info.minImageCount,
        .imageFormat            = info.surfaceFormat.format,
        .imageColorSpace        = info.surfaceFormat.colorSpace,
        .imageExtent            = info.extent,
        .imageArrayLayers       = info.imageArrayLayers,
        .imageUsage             = info.usage,
        .imageSharingMode       = info.queueFamily.size32() == 0u
            ? VK_SHARING_MODE_EXCLUSIVE
            : VK_SHARING_MODE_CONCURRENT,
        .queueFamilyIndexCount  = info.queueFamily.size32(),
        .pQueueFamilyIndices    = info.queueFamily.data(),
        .preTransform           = info.preTransform,
        .compositeAlpha         = info.compositeAlpha,
        .presentMode            = info.presentMode,
        .clipped                = info.clipped,
        .oldSwapchain           = info.oldSwapchain,
    };
    VkSwapchainKHR swapchain = VK_NULL_HANDLE;
    VkResult const result = vkCreateSwapchainKHR(device, &swapchainCreateInfo, pAlloc, &swapchain);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateSwapchainKHR"};
    return
    {
        swapchain,
        [device](VkSwapchainKHR const handle) noexcept {vkDestroySwapchainKHR(device, handle, pAlloc);}
    };
}

vk::utils::array<VkImage> getSwapchainImages(VkDevice const device, VkSwapchainKHR const swapchain)
{
    vk::u32 imageCount;
    vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);
    vk::utils::array<VkImage> image(imageCount);
    vkGetSwapchainImagesKHR(device, swapchain, &imageCount, image.data());
    return image;
}
#endif

} // namespace vk::khr
