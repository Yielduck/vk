#include <vk/khr/pipeline_executable_info.h>
#include <vk/utils/proc_addr.h>
#include <ranges>
namespace vk::khr
{

#ifdef VK_KHR_pipeline_executable_properties
vk::utils::array<vk::khr::PipelineExecutableInfo> pipelineExecutableInfo(VkDevice const device, VkPipeline const pipeline)
{
    auto const getExecProperties = deviceProcAddr(device, vkGetPipelineExecutablePropertiesKHR);
    auto const getExecStatistics = deviceProcAddr(device, vkGetPipelineExecutableStatisticsKHR);
    if(getExecProperties == nullptr)
        return {};

    VkPipelineInfoKHR const pipelineInfo =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR,
        .pNext = nullptr,
        .pipeline = pipeline,
    };
    vk::u32 propCount = 0u;
    getExecProperties(device, &pipelineInfo, &propCount, nullptr);
    vk::utils::array<VkPipelineExecutablePropertiesKHR> execProp(propCount);
    for(auto &prop : execProp)
        prop.sType = VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR;
    getExecProperties(device, &pipelineInfo, &propCount, execProp.data());

    auto const getExecStats = [&](vk::u32 const i) noexcept
        -> vk::utils::array<VkPipelineExecutableStatisticKHR>
    {
        if(getExecStatistics == nullptr)
            return {};

        VkPipelineExecutableInfoKHR const executableInfo =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR,
            .pNext = nullptr,
            .pipeline = pipeline,
            .executableIndex = i,
        };
        vk::u32 statCount = 0u;
        getExecStatistics(device, &executableInfo, &statCount, nullptr);
        vk::utils::array<VkPipelineExecutableStatisticKHR> stats(statCount);
        for(auto &stat : stats)
            stat.sType = VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR;
        getExecStatistics(device, &executableInfo, &statCount, stats.data());
        return stats;
    };
    return {std::views::iota(0u, propCount) | std::views::transform
    (
        [&](vk::u32 const i) noexcept
            -> vk::khr::PipelineExecutableInfo
        {
            return
            {
                .properties = execProp[i],
                .statistics = getExecStats(i),
            };
        }
    )};
}
#endif

} // namespace vk::khr
