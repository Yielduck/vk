#include <vk/khr/queue_present.h>
namespace vk::khr
{

#ifdef VK_KHR_swapchain
VkResult queuePresent(VkQueue const queue, vk::khr::PresentInfo const &info)
{
    VkPresentInfoKHR const presentInfo =
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext              = info.chain,
        .waitSemaphoreCount = info.waitSemaphore.size32(),
        .pWaitSemaphores    = info.waitSemaphore.data(),
        .swapchainCount     = 1u,
        .pSwapchains        = &info.swapchain,
        .pImageIndices      = &info.imageIndex,
        .pResults           = nullptr,
    };
    VkResult const result = vkQueuePresentKHR(queue, &presentInfo);
    if(VK_SUCCESS != result && VK_SUBOPTIMAL_KHR != result)
        throw vk::Exception{result, "vkQueuePresentKHR"};
    return result;
}
#endif

} // namespace vk::khr
