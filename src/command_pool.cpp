#include <vk/command_pool.h>
namespace vk
{

vk::CommandPool createCommandPool(VkDevice const device, vk::CommandPoolCreateInfo const &info)
{
    VkCommandPoolCreateInfo const commandPoolCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext              = info.chain,
        .flags              = info.flags,
        .queueFamilyIndex   = info.queueFamilyIndex,
    };
    VkCommandPool commandPool = VK_NULL_HANDLE;
    VkResult const result = vkCreateCommandPool(device, &commandPoolCreateInfo, pAlloc, &commandPool);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateCommandPool"};
    return
    {
        commandPool,
        [device](VkCommandPool const handle) noexcept {vkDestroyCommandPool(device, handle, pAlloc);},
    };
}

} // namespace vk
