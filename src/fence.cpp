#include <vk/fence.h>
namespace vk
{

Fence createFence(VkDevice const device, vk::FenceCreateInfo const &info)
{
    VkFenceCreateInfo const fenceCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = info.chain,
        .flags = info.flags,
    };
    VkFence fence = VK_NULL_HANDLE;
    VkResult const result = vkCreateFence(device, &fenceCreateInfo, pAlloc, &fence);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateFence"};
    return
    {
        fence,
        [device](VkFence const handle) noexcept {vkDestroyFence(device, handle, pAlloc);},
    };
}

VkResult waitForFences
(
    VkDevice const device,
    vk::utils::span<VkFence const> const fence,
    VkBool32 const waitAll,
    vk::u64 const timeout
)
{
    VkResult const result = vkWaitForFences(device, fence.size32(), fence.data(), waitAll, timeout);
    if(VK_SUCCESS != result && VK_TIMEOUT != result)
        throw vk::Exception{result, "vkWaitForFences"};
    return result;
}
void resetFences(VkDevice const device, vk::utils::span<VkFence const> const fence)
{
    VkResult const result = vkResetFences(device, fence.size32(), fence.data());
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkResetFences"};
}

} // namespace vk
