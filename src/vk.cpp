#include <vk/vk.h>
#include <atomic>
#include <memory>
#include <memory_resource>
namespace vk
{

VkAllocationCallbacks *pAlloc = nullptr;

// std::pmr::monotonic_buffer_resource is not thread-safe, so
class MonotonicBuffer final : public std::pmr::memory_resource
{
    alignas(64) std::atomic<std::size_t> allocated;

    char *memory;
    std::size_t capacity;

public:
    MonotonicBuffer(void *buffer, std::size_t size)
        : allocated(0)
        , memory(static_cast<char *>(buffer))
        , capacity(size)
    {}

protected:
    virtual void *do_allocate(std::size_t const bytes, std::size_t const alignment) override
    {
        void *result;
        std::size_t offset = allocated.load(std::memory_order::relaxed);

        do
        {
            result = memory + offset;
            std::size_t space = capacity - offset;
            if(nullptr == std::align(alignment, bytes, result, space))
                throw std::bad_alloc{};
        } while(!allocated.compare_exchange_weak( offset
                                                , static_cast<std::size_t>(static_cast<char *>(result) - memory) + bytes
                                                , std::memory_order::relaxed
                                                , std::memory_order::relaxed
                                                ));

        return result;
    }
    virtual void do_deallocate(void *, std::size_t, std::size_t) override
    {
    }
    virtual bool do_is_equal(std::pmr::memory_resource const &other) const noexcept override
    {
        return this == dynamic_cast<MonotonicBuffer const *>(&other);
    }
};

// std::pmr::synchronized_pool_resource has no fine control of pool block size, so
template<std::size_t BlockSize, std::size_t BlockAlign>
class Pool final : public std::pmr::memory_resource
{
    struct Node
    {
        Node *next;
        char memory[BlockSize - sizeof(Node *)];
    };
    static_assert(sizeof(Node) == BlockSize);

    alignas(64) std::atomic<Node *> head;
    std::pmr::memory_resource *upstream;

public:

    Pool(std::size_t const initialBlocks = 0u, std::pmr::memory_resource * const res = std::pmr::get_default_resource())
        : upstream(res)
    {
        Node *curr = nullptr;
        for(std::size_t i = 0u; i < initialBlocks; ++i)
        {
            Node * const node = static_cast<Node *>(upstream->allocate(BlockSize, BlockAlign));
            node->next = curr;
            curr = node;
            head.store(node, std::memory_order::relaxed);
        }
        head.store(curr, std::memory_order::relaxed);
    }
    ~Pool()
    {
        Node *curr = head.load(std::memory_order::relaxed);
        while(curr != nullptr)
        {
            Node * const next = curr->next;
            upstream->deallocate(curr, BlockSize, BlockAlign);
            curr = next;
        }
    }

protected:
    virtual void *do_allocate(std::size_t const bytes, std::size_t const alignment) override
    {
        if(bytes > BlockSize || BlockAlign % alignment != 0u)
            throw std::bad_alloc{};

        Node *node = head.load(std::memory_order::relaxed);
        do
        {
            if(node == nullptr)
                return upstream->allocate(BlockSize, BlockAlign);
        } while(!head.compare_exchange_weak( node
                                           , node->next
                                           , std::memory_order::relaxed
                                           , std::memory_order::relaxed
                                           ));
        return node;
    }
    virtual void do_deallocate(void * const pointer, std::size_t, std::size_t) override
    {
        Node * const node = static_cast<Node *>(pointer);
        // data race:
        while(!head.compare_exchange_weak( node->next
                                         , node
                                         , std::memory_order::release
                                         , std::memory_order::relaxed
                                         ))
            ;
    }
    virtual bool do_is_equal(std::pmr::memory_resource const &other) const noexcept override
    {
        return this == dynamic_cast<Pool<BlockSize, BlockAlign> const *>(&other);
    }
};

template<std::size_t N>
class Segregator final : public std::pmr::memory_resource
{
    std::pmr::memory_resource *primary;
    std::pmr::memory_resource *fallback;

public:
    Segregator( std::pmr::memory_resource *res0
              , std::pmr::memory_resource *res1 = std::pmr::get_default_resource()
              ) noexcept
        :  primary(res0)
        , fallback(res1)
    {}
protected:
    virtual void *do_allocate(std::size_t const bytes, std::size_t const alignment) override
    {
        return bytes <= N
            ?  primary->allocate(bytes, alignment)
            : fallback->allocate(bytes, alignment);
    }
    virtual void do_deallocate(void * const pointer, std::size_t const bytes, std::size_t const alignment) override
    {
        return bytes <= N
            ?  primary->deallocate(pointer, bytes, alignment)
            : fallback->deallocate(pointer, bytes, alignment);
    }
    virtual bool do_is_equal(std::pmr::memory_resource const &other) const noexcept override
    {
        return this == dynamic_cast<Segregator<N> const *>(&other);
    }
};

alignas(4096) char vkMemoryResource[VK_STATIC_MEMORY_SIZE];

auto const fallback = VK_NO_HEAP
    ? std::pmr::null_memory_resource()
    : std::pmr::get_default_resource();

/* stdlib version:
std::pmr::monotonic_buffer_resource monotonic(vkMemoryResource, sizeof(vkMemoryResource), fallback);
std::pmr::synchronized_pool_resource stdpool({.max_blocks_per_chunk = 32, .largest_required_pool_block = 32768}, &monotonic);
std::pmr::polymorphic_allocator<char> polyalloc(&stdpool);
*/

MonotonicBuffer monotonic(vkMemoryResource, sizeof(vkMemoryResource));

Pool<32768, 128> gigapool(  1u, &monotonic); // 0x00000:0x08000
Pool< 4096, 128>  bigpool(  2u, &monotonic); // 0x08000:0x0a000
Pool<  512, 128>  midpool( 16u, &monotonic); // 0x0a000:0x0c000
Pool<  128, 128>  lilpool( 32u, &monotonic); // 0x0c000:0x0e000
Pool<   32,  32> minipool(256u, &monotonic); // 0x0e000:0x10000
static_assert(sizeof(vkMemoryResource) >= 65536);

Segregator<32768>         gigaToFallback(&gigapool,        fallback);
Segregator< 4096>          bigToFallback(& bigpool, &gigaToFallback);
Segregator<  512>          midToFallback(& midpool, & bigToFallback);
Segregator<  128>          lilToFallback(& lilpool, & midToFallback);
Segregator<   32> smartAssMemoryResource(&minipool, & lilToFallback);

std::pmr::polymorphic_allocator<char> polyalloc(&smartAssMemoryResource);

void *allocate(std::size_t const bytes, std::size_t const alignment)
{
    return polyalloc.allocate_bytes(bytes, alignment);
}
void deallocate(void * const pointer, std::size_t const bytes, std::size_t const alignment) noexcept
{
    return polyalloc.deallocate_bytes(pointer, bytes, alignment);
}

} // namespace vk
