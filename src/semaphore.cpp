#include <vk/semaphore.h>
namespace vk
{

vk::Semaphore createSemaphore(VkDevice const device, vk::SemaphoreCreateInfo const &info)
{
    VkSemaphoreCreateInfo const semaphoreCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = info.chain,
        .flags = info.flags,
    };
    VkSemaphore semaphore = VK_NULL_HANDLE;
    VkResult const result = vkCreateSemaphore(device, &semaphoreCreateInfo, pAlloc, &semaphore);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateSemaphore"};
    return
    {
        semaphore,
        [device](VkSemaphore const handle) noexcept {vkDestroySemaphore(device, handle, pAlloc);}
    };
}

} // namespace vk
