#include <vk/render_pass.h>
#include <vk/utils/array.h>
namespace vk
{

vk::RenderPass createRenderPass(VkDevice const device, vk::RenderPassCreateInfo const &info)
{
    vk::utils::array<VkSubpassDescription> subpassDescription(info.subpass.size());
    for(vk::u32 i = 0u; i < subpassDescription.size32(); ++i)
        subpassDescription[i] =
        {
            .flags                      = info.subpass[i].flags,
            .pipelineBindPoint          = info.subpass[i].bindPoint,
            .inputAttachmentCount       = info.subpass[i].inputAttachment.size32(),
            .pInputAttachments          = info.subpass[i].inputAttachment.data(),
            .colorAttachmentCount       = info.subpass[i].colorAttachment.size32(),
            .pColorAttachments          = info.subpass[i].colorAttachment.data(),
            .pResolveAttachments        = info.subpass[i].resolveAttachment.data(),
            .pDepthStencilAttachment    = &info.subpass[i].depthStencilAttachment,
            .preserveAttachmentCount    = info.subpass[i].preserveAttachment.size32(),
            .pPreserveAttachments       = info.subpass[i].preserveAttachment.data(),
        };
    VkRenderPassCreateInfo const renderPassCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext              = info.chain,
        .flags              = info.flags,
        .attachmentCount    = info.attachment.size32(),
        .pAttachments       = info.attachment.data(),
        .subpassCount       = subpassDescription.size32(),
        .pSubpasses         = subpassDescription.data(),
        .dependencyCount    = info.dependency.size32(),
        .pDependencies      = info.dependency.data(),
    };
    VkRenderPass renderPass = VK_NULL_HANDLE;
    VkResult const result = vkCreateRenderPass(device, &renderPassCreateInfo, pAlloc, &renderPass);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateRenderPass"};
    return
    {
        renderPass,
        [device](VkRenderPass const handle) noexcept {vkDestroyRenderPass(device, handle, pAlloc);}
    };
}

} // namespace vk
