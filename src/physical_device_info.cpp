#include <vk/physical_device_info.h>
namespace vk
{

vk::utils::array<VkPhysicalDevice> enumeratePhysicalDevices(VkInstance const instance)
{
    vk::u32 deviceCount = 0;
    {
        VkResult const result = vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
        if(VK_SUCCESS != result)
            throw vk::Exception{result, "vkEnumeratePhysicalDevices"};
    }

    vk::utils::array<VkPhysicalDevice> device(deviceCount);
    VkResult const result = vkEnumeratePhysicalDevices(instance, &deviceCount, device.data());
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkEnumeratePhysicalDevices"};
    return device;
}
vk::PhysicalDeviceInfo physicalDeviceInfo(VkPhysicalDevice const device)
{
    vk::PhysicalDeviceInfo info;
    vkGetPhysicalDeviceProperties(device, &info.properties);
    vkGetPhysicalDeviceFeatures  (device, &info.features  );

    VkPhysicalDeviceMemoryProperties memoryProps;
    vkGetPhysicalDeviceMemoryProperties(device, &memoryProps);
    info.memoryProperties = vk::utils::array<vk::PhysicalDeviceMemoryProperties>(memoryProps.memoryTypeCount);
    for(vk::u32 i = 0u; i < memoryProps.memoryTypeCount; ++i)
    {
        VkMemoryType const type = memoryProps.memoryTypes[i];
        VkMemoryHeap const heap = memoryProps.memoryHeaps[type.heapIndex];
        info.memoryProperties[i] = {type, heap};
    }

    vk::u32 queueCount = 0u;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, nullptr);
    info.queueProperties = vk::utils::array<VkQueueFamilyProperties>(queueCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueCount, info.queueProperties.data());

    vk::u32 extCount = 0u;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extCount, nullptr);
    info.extensionProperties = vk::utils::array<VkExtensionProperties>(extCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extCount, info.extensionProperties.data());
    return info;
}

#ifdef VK_VERSION_1_1
vk::utils::StructureChain getPhysicalDeviceProperties2(VkPhysicalDevice const device, vk::utils::StructureChain properties) noexcept
{
    VkPhysicalDeviceProperties2 props =
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
        .pNext = properties,
        .properties = {},
    };
    vkGetPhysicalDeviceProperties2(device, &props);
    return properties;
}
vk::utils::StructureChain getPhysicalDeviceFeatures2(VkPhysicalDevice const device, vk::utils::StructureChain features) noexcept
{
    VkPhysicalDeviceFeatures2 feats =
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = features,
        .features = {},
    };
    vkGetPhysicalDeviceFeatures2(device, &feats);
    return features;
}
#endif

} // namespace vk
