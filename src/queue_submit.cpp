#include <vk/queue_submit.h>
#include <vk/utils/array.h>
namespace vk
{

void queueSubmit(VkQueue const queue, vk::utils::span<SubmitInfo const> const info, VkFence const fence)
{
    vk::u32 const submitCount = info.size32();
    vk::utils::array<VkSubmitInfo> submitInfo(submitCount);
    for(vk::u32 i = 0u; i < submitCount; ++i)
        submitInfo[i] =
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext                  = info[i].chain,
            .waitSemaphoreCount     = info[i].waitSemaphore.size32(),
            .pWaitSemaphores        = info[i].waitSemaphore.data(),
            .pWaitDstStageMask      = info[i].waitDstStageMask.data(),
            .commandBufferCount     = info[i].commandBuffer.size32(),
            .pCommandBuffers        = info[i].commandBuffer.data(),
            .signalSemaphoreCount   = info[i].signalSemaphore.size32(),
            .pSignalSemaphores      = info[i].signalSemaphore.data(),
        };
    VkResult const result = vkQueueSubmit(queue, submitCount, submitInfo.data(), fence);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkQueueSubmit"};
}

} // namespace vk
