#include <vk/instance.h>
namespace vk
{

vk::utils::array<VkExtensionProperties> enumerateInstanceExtensionProperties(char const * const layerName)
{
    vk::u32 propertyCount = 0u;
    vkEnumerateInstanceExtensionProperties(layerName, &propertyCount, nullptr);
    vk::utils::array<VkExtensionProperties> properties(propertyCount);
    VkResult const result = vkEnumerateInstanceExtensionProperties(layerName, &propertyCount, properties.data());
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkEnumerateInstanceExtensionProperties"};
    return properties;
}

vk::Instance createInstance(vk::InstanceCreateInfo const &info)
{
    VkApplicationInfo const appInfo =
    {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext              = nullptr,
        .pApplicationName   = info.applicationName,
        .applicationVersion = info.applicationVersion,
        .pEngineName        = info.engineName,
        .engineVersion      = info.engineVersion,
        .apiVersion         = info.apiVersion,
    };
    VkInstanceCreateInfo const createInfo =
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext                      = info.chain,
        .flags                      = info.flags,
        .pApplicationInfo           = &appInfo,
        .enabledLayerCount          = info.validationLayer.size32(),
        .ppEnabledLayerNames        = info.validationLayer.data(),
        .enabledExtensionCount      = info.extension.size32(),
        .ppEnabledExtensionNames    = info.extension.data(),
    };
    VkInstance instance = VK_NULL_HANDLE;
    VkResult const result = vkCreateInstance(&createInfo, pAlloc, &instance);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateInstance"};
    return
    {
        instance,
        +[](VkInstance const handle) noexcept {vkDestroyInstance(handle, pAlloc);},
    };
}

} // namespace vk
