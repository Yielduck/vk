#include <vk/device.h>
#include <vk/utils/array.h>
namespace vk
{

vk::Device createDevice(VkPhysicalDevice const physicalDevice, vk::DeviceCreateInfo const &info)
{
    vk::utils::array<VkDeviceQueueCreateInfo> deviceQueueCreateInfo(info.queueInfo.size());
    for(vk::u32 i = 0u; i < deviceQueueCreateInfo.size32(); ++i)
        deviceQueueCreateInfo[i] =
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext              = info.queueInfo[i].chain,
            .flags              = info.queueInfo[i].flags,
            .queueFamilyIndex   = info.queueInfo[i].queueFamilyIndex,
            .queueCount         = info.queueInfo[i].priority.size32(),
            .pQueuePriorities   = info.queueInfo[i].priority.data(),
        };

    VkDeviceCreateInfo const deviceCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext                      = info.chain,
        .flags                      = info.flags,
        .queueCreateInfoCount       = deviceQueueCreateInfo.size32(),
        .pQueueCreateInfos          = deviceQueueCreateInfo.data(),
        .enabledLayerCount          = 0,       // deprecated and ignored
        .ppEnabledLayerNames        = nullptr, // deprecated and ignored
        .enabledExtensionCount      = info.extension.size32(),
        .ppEnabledExtensionNames    = info.extension.data(),
        .pEnabledFeatures           = info.enabledFeatures,
    };
    VkDevice device = VK_NULL_HANDLE;
    VkResult const result = vkCreateDevice(physicalDevice, &deviceCreateInfo, pAlloc, &device);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateDevice"};
    return
    {
        device,
        +[](VkDevice const handle) noexcept
        {
            vkDestroyDevice(handle, pAlloc);
        }
    };
}
VkQueue getDeviceQueue(VkDevice const device, vk::u32 const family, vk::u32 const i) noexcept
{
    VkQueue queue = VK_NULL_HANDLE;
    vkGetDeviceQueue(device, family, i, &queue);
    return queue;
}

} // namespace vk
