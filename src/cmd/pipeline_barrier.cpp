#include <vk/cmd/pipeline_barrier.h>
#include <vk/utils/array.h>
namespace vk::cmd
{

void pipelineBarrier(VkCommandBuffer const commandBuffer, vk::PipelineBarrierInfo const &info)
{
    vk::utils::array<      VkMemoryBarrier> memoryBarrier(info.memoryBarrier.size());
    vk::utils::array<VkBufferMemoryBarrier> bufferBarrier(info.bufferBarrier.size());
    vk::utils::array< VkImageMemoryBarrier>  imageBarrier(info. imageBarrier.size());

    for(vk::u32 i = 0u; i < memoryBarrier.size32(); ++i)
        memoryBarrier[i] =
        {
            .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
            .pNext          = info.memoryBarrier[i].chain,
            .srcAccessMask  = info.memoryBarrier[i].srcAccessMask,
            .dstAccessMask  = info.memoryBarrier[i].dstAccessMask,
        };
    for(vk::u32 i = 0u; i < bufferBarrier.size32(); ++i)
        bufferBarrier[i] =
        {
            .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
            .pNext                  = info.bufferBarrier[i].chain,
            .srcAccessMask          = info.bufferBarrier[i].srcAccessMask,
            .dstAccessMask          = info.bufferBarrier[i].dstAccessMask,
            .srcQueueFamilyIndex    = info.bufferBarrier[i].srcQueueFamilyIndex,
            .dstQueueFamilyIndex    = info.bufferBarrier[i].dstQueueFamilyIndex,
            .buffer                 = info.bufferBarrier[i].buffer,
            .offset                 = info.bufferBarrier[i].offset,
            .size                   = info.bufferBarrier[i].size,
        };
    for(vk::u32 i = 0u; i < imageBarrier.size32(); ++i)
        imageBarrier[i] =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext                  = info.imageBarrier[i].chain,
            .srcAccessMask          = info.imageBarrier[i].srcAccessMask,
            .dstAccessMask          = info.imageBarrier[i].dstAccessMask,
            .oldLayout              = info.imageBarrier[i].oldLayout,
            .newLayout              = info.imageBarrier[i].newLayout,
            .srcQueueFamilyIndex    = info.imageBarrier[i].srcQueueFamilyIndex,
            .dstQueueFamilyIndex    = info.imageBarrier[i].dstQueueFamilyIndex,
            .image                  = info.imageBarrier[i].image,
            .subresourceRange       = info.imageBarrier[i].subresourceRange,
        };

    vkCmdPipelineBarrier
    (
        commandBuffer,
        info.srcStageMask,
        info.dstStageMask,
        info.dependencyFlags,
        memoryBarrier.size32(),
        memoryBarrier.data  (),
        bufferBarrier.size32(),
        bufferBarrier.data  (),
         imageBarrier.size32(),
         imageBarrier.data  ()
    );
}

} // namespace vk::cmd
