#include <vk/cmd/copy.h>
namespace vk::cmd
{

void copyBuffer(VkCommandBuffer const commandBuffer, CopyBufferInfo const &info) noexcept
{
    vkCmdCopyBuffer
    (
        commandBuffer,
        info.srcBuffer,
        info.dstBuffer,
        info.region.size32(),
        info.region.data()
    );
}
void copyImage(VkCommandBuffer const commandBuffer, CopyImageInfo const &info) noexcept
{
    vkCmdCopyImage
    (
        commandBuffer,
        info.srcImage,
        info.srcImageLayout,
        info.dstImage,
        info.dstImageLayout,
        info.region.size32(),
        info.region.data()
    );
}
void copyBufferToImage(VkCommandBuffer const commandBuffer, CopyBufferToImageInfo const &info) noexcept
{
    vkCmdCopyBufferToImage
    (
        commandBuffer,
        info.srcBuffer,
        info.dstImage,
        info.dstImageLayout,
        info.region.size32(),
        info.region.data()
    );
}
void copyImageToBuffer(VkCommandBuffer const commandBuffer, CopyImageToBufferInfo const &info) noexcept
{
    vkCmdCopyImageToBuffer
    (
        commandBuffer,
        info.srcImage,
        info.srcImageLayout,
        info.dstBuffer,
        info.region.size32(),
        info.region.data()
    );
}

} // namespace vk::cmd
