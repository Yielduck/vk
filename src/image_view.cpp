#include <vk/image_view.h>
namespace vk
{

vk::ImageView createImageView(VkDevice const device, vk::ImageViewCreateInfo const &info)
{
    VkImageViewCreateInfo const imageViewCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext              = info.chain,
        .flags              = info.flags,
        .image              = info.image,
        .viewType           = info.type,
        .format             = info.format,
        .components         = info.components,
        .subresourceRange   = info.subresourceRange,
    };
    VkImageView view = VK_NULL_HANDLE;
    VkResult const result = vkCreateImageView(device, &imageViewCreateInfo, pAlloc, &view);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateImageView"};
    return
    {
        view,
        [device](VkImageView const handle) noexcept {vkDestroyImageView(device, handle, pAlloc);},
    };
}

} // namespace vk
