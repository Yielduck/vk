#include <vk/buffer_view.h>
namespace vk
{

vk::BufferView createBufferView(VkDevice const device, vk::BufferViewCreateInfo const &info)
{
    VkBufferViewCreateInfo const bufferViewCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO,
        .pNext  = info.chain,
        .flags  = {},
        .buffer = info.buffer,
        .format = info.format,
        .offset = info.offset,
        .range  = info.range,
    };
    VkBufferView view = VK_NULL_HANDLE;
    VkResult const result = vkCreateBufferView(device, &bufferViewCreateInfo, pAlloc, &view);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateBufferView"};
    return
    {
        view,
        [device](VkBufferView const handle) noexcept {vkDestroyBufferView(device, handle, pAlloc);},
    };
}

} // namespace vk
