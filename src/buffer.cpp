#include <vk/buffer.h>
namespace vk
{

vk::Buffer createBuffer(VkDevice const device, vk::BufferCreateInfo const &info)
{
    if(info.size == 0u)
        return {};
    VkBufferCreateInfo const bufferCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                  = info.chain,
        .flags                  = info.flags,
        .size                   = info.size,
        .usage                  = info.usage,
        .sharingMode            = info.queueFamily.size32() == 0u
            ? VK_SHARING_MODE_EXCLUSIVE
            : VK_SHARING_MODE_CONCURRENT,
        .queueFamilyIndexCount  = info.queueFamily.size32(),
        .pQueueFamilyIndices    = info.queueFamily.data(),
    };
    VkBuffer buffer = VK_NULL_HANDLE;
    VkResult const result = vkCreateBuffer(device, &bufferCreateInfo, pAlloc, &buffer);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateBuffer"};
    return
    {
        buffer,
        [device](VkBuffer const handle) noexcept {vkDestroyBuffer(device, handle, pAlloc);},
    };
}
VkMemoryRequirements bufferMemoryRequirements(VkDevice const device, VkBuffer const buffer) noexcept
{
    VkMemoryRequirements requirements;
    vkGetBufferMemoryRequirements(device, buffer, &requirements);
    return requirements;
}
void bindBufferMemory
(
    VkDevice const device,
    VkBuffer const buffer,
    VkDeviceMemory const memory,
    VkDeviceSize const offset
)
{
    VkResult const result = vkBindBufferMemory(device, buffer, memory, offset);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkBindBufferMemory"};
}

} // namespace vk
