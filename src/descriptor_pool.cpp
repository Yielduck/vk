#include <vk/descriptor_pool.h>
namespace vk
{

vk::DescriptorPool createDescriptorPool(VkDevice const device, vk::DescriptorPoolCreateInfo const &info)
{
    VkDescriptorPoolCreateInfo const descriptorPoolCreateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext          = info.chain,
        .flags          = info.flags,
        .maxSets        = info.maxSets,
        .poolSizeCount  = info.poolSize.size32(),
        .pPoolSizes     = info.poolSize.data  (),
    };
    VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
    VkResult const result = vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, pAlloc, &descriptorPool);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkCreateDescriptorPool"};
    return
    {
        descriptorPool,
        [device](VkDescriptorPool const handle) noexcept {vkDestroyDescriptorPool(device, handle, pAlloc);},
    };
}

} // namespace vk
