#include <vk/descriptor_set.h>
namespace vk
{

vk::DescriptorSet createDescriptorSet(VkDevice const device, vk::DescriptorSetCreateInfo const &info)
{
    VkDescriptorSetAllocateInfo const descriptorSetAllocateInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext              = info.chain,
        .descriptorPool     = info.descriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts        = &info.setLayout,
    };
    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    VkResult const result = vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet);
    if(VK_SUCCESS != result)
        throw vk::Exception{result, "vkAllocateDescriptorSets"};
    return
    {
        descriptorSet,
        [
            device,
            pool = info.descriptorPool,
            free = info.freeDescriptorSet
        ](VkDescriptorSet const handle) noexcept
        {
            if(free)
                vkFreeDescriptorSets(device, pool, 1, &handle);
        },
    };
}

namespace writeDescriptorSet
{

inline VkWriteDescriptorSet image
(
    VkDescriptorSet const descriptorSet,
    vk::u32 const binding,
    VkDescriptorType const descriptorType,
    vk::utils::span<VkDescriptorImageInfo const> const imageInfo
)
{
    return
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet             = descriptorSet,
        .dstBinding         = binding,
        .dstArrayElement    = 0,
        .descriptorCount    = imageInfo.size32(),
        .descriptorType     = descriptorType,
        .pImageInfo         = imageInfo.data(),
        .pBufferInfo        = nullptr,
        .pTexelBufferView   = nullptr,
    };
}
inline VkWriteDescriptorSet buffer
(
    VkDescriptorSet const descriptorSet,
    vk::u32 const binding,
    VkDescriptorType const descriptorType,
    vk::utils::span<VkDescriptorBufferInfo const> const bufferInfo
)
{
    return
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet             = descriptorSet,
        .dstBinding         = binding,
        .dstArrayElement    = 0,
        .descriptorCount    = bufferInfo.size32(),
        .descriptorType     = descriptorType,
        .pImageInfo         = nullptr,
        .pBufferInfo        = bufferInfo.data(),
        .pTexelBufferView   = nullptr,
    };
}
inline VkWriteDescriptorSet texelBuffer
(
    VkDescriptorSet const descriptorSet,
    vk::u32 const binding,
    VkDescriptorType const descriptorType,
    vk::utils::span<VkBufferView const> const texelBufferInfo
)
{
    return
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet             = descriptorSet,
        .dstBinding         = binding,
        .dstArrayElement    = 0,
        .descriptorCount    = texelBufferInfo.size32(),
        .descriptorType     = descriptorType,
        .pImageInfo         = nullptr,
        .pBufferInfo        = nullptr,
        .pTexelBufferView   = texelBufferInfo.data(),
    };
}

VkWriteDescriptorSet sampler(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorImageInfo const> const info) noexcept
{
    return image(set, i, VK_DESCRIPTOR_TYPE_SAMPLER, info);
}
VkWriteDescriptorSet combinedImageSampler(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorImageInfo const> const info) noexcept
{
    return image(set, i, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, info);
}
VkWriteDescriptorSet sampledImage(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorImageInfo const> const info) noexcept
{
    return image(set, i, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, info);
}
VkWriteDescriptorSet storageImage(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorImageInfo const> const info) noexcept
{
    return image(set, i, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, info);
}
VkWriteDescriptorSet inputAttachment(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorImageInfo const> const info) noexcept
{
    return image(set, i, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, info);
}
VkWriteDescriptorSet storageBuffer(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorBufferInfo const> const info) noexcept
{
    return buffer(set, i, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, info);
}
VkWriteDescriptorSet uniformBuffer(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkDescriptorBufferInfo const> const info) noexcept
{
    return buffer(set, i, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, info);
}
VkWriteDescriptorSet storageTexelBuffer(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkBufferView const> const info) noexcept
{
    return texelBuffer(set, i, VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, info);
}
VkWriteDescriptorSet uniformTexelBuffer(VkDescriptorSet const set, vk::u32 const i, vk::utils::span<VkBufferView const> const info) noexcept
{
    return texelBuffer(set, i, VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, info);
}

} // namespace writeDescriptorSet

void updateDescriptorSets
(
    VkDevice const device,
    vk::utils::span<VkWriteDescriptorSet const> const writeDescriptorSet,
    vk::utils::span< VkCopyDescriptorSet const> const copyDescriptorSet
)
{
    vkUpdateDescriptorSets
    (
        device,
        writeDescriptorSet.size32(),
        writeDescriptorSet.data(),
         copyDescriptorSet.size32(),
         copyDescriptorSet.data()
    );
}

} // namespace vk
