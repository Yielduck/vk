This is yet another attempt to wrap some Vulkan API functionality into C++ structures and functions.
The wrappers are designed to reduce the amount of boilerplate by using sensible defaults and designated initializers, C++-20 feature.
The implementation provides

* vk::[...]CreateInfo structures with:
    * No need to fill .sType by hand
    * Easy way to push a tuple into .pNext
    * Default values where they make sense
    * Spans instead of (size, pointer) pairs
    * Automatic choice between VK_SHARING_MODE_[EXCLUSIVE | CONCURRENT] based on emptiness of queue family share span

* vk::create[...] functions throwing vk::Exception{result, funcName} and returning RAII handles

* Guards for memory maps, command buffers and render passes

* Handy utilities for creating simple vulkan instance, device, framebuffer and swap chains, GLFW surface and ImGUI context

Including core library headers may include only lightweight standard C++ headers, such as \<cstdint\>, \<cstddef\>, \<cassert\> and \<new\>.

If possible, the library won't use the heap for memory allocation -- instead, it provides a custom allocation strategy from a fixed-size buffer.
